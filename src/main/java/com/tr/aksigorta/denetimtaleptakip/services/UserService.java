package com.tr.aksigorta.denetimtaleptakip.services;

import com.tr.aksigorta.denetimtaleptakip.model.Role;
import com.tr.aksigorta.denetimtaleptakip.model.User;

import java.util.List;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:04 PM
 */
public interface UserService extends BaseService
{
    public User findUser(String username);

    public List<User> retrieveActiveUsers();

    public User mergeUser(User user);

    public List<User> retrieveAllUsers();

    public List<Role> retrieveAllRoles();
}
