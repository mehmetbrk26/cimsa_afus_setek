package com.tr.aksigorta.denetimtaleptakip.services.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.sun.org.apache.xpath.internal.operations.Bool;
import com.tr.aksigorta.denetimtaleptakip.controller.util.Utility;
import com.tr.aksigorta.denetimtaleptakip.model.*;
import com.tr.aksigorta.denetimtaleptakip.services.FindingService;
import com.tr.aksigorta.denetimtaleptakip.services.MailService;
import com.tr.aksigorta.denetimtaleptakip.services.util.SpringApplicationContextUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StopWatch;
import org.stringtemplate.v4.ST;

import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * User: mhazer
 * Date: 8/22/12
 * Time: 5:02 PM
 */
@Service("mailService")
public class MailServiceImpl implements MailService {
    private final Logger logger = LoggerFactory.getLogger(MailServiceImpl.class);

    private FindingService findingService;

    private JavaMailSender javaMailSender;

    private SimpleMailMessage templateMailMessage;

    private String auditSystemURL;
    private Boolean enableEmails;
    private Boolean batchJobEnabled;

    @Override
    @Async
    @Transactional
    public void sendNotificationEmails() {
        if ( batchJobEnabled ) {
            StopWatch stopWatch = new StopWatch("sending finding emails");
            stopWatch.start();
            logger.debug("scanning findings and sending mails");
            List<Finding> notCompletedFindings = findingService.retrieveNotCompletedFindings();
            logger.debug("there are {} not completed findings", notCompletedFindings.size());
            for (Finding notCompletedFinding : notCompletedFindings) {
                try {
                    Integer findingId = notCompletedFinding.getAuditType().getEmailSendingStrategy().getId();
                    if (EmailSendingStrategyEnum.THREE_MONTH_STRATEGY.getCode().equals(findingId)) {
                        this.executeThreeMonthsEmailStrategy(notCompletedFinding);
                    } else if (EmailSendingStrategyEnum.ONE_DAY_BEFORE_STRATEGY.getCode().equals(findingId)) {
                        this.executeOneDayBeforeEmailStrategy(notCompletedFinding);
                    } else if (EmailSendingStrategyEnum.ENERJISA_STRATEGY.getCode().equals(findingId)) {
                        this.executeEnerjisaEmailStrategy(notCompletedFinding);
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                    logger.error("mail sending job failed {}", exception.getMessage());
                }
            }
            stopWatch.stop();
            logger.info("total elapsed time {}", stopWatch.prettyPrint());
        }
    }

    private void executeThreeMonthsEmailStrategy(Finding notCompletedFinding) {
        LocalDate today = new LocalDate();
        LocalDate findingOpeningDate = new LocalDate(notCompletedFinding.getAuditDate());
        Period period = new Period(findingOpeningDate, today);
        if (!findingOpeningDate.equals(today) && period.getMonths() % 3 == 0) {
            if (period.getWeeks() == 0 && period.getDays() == 0) {
                logger.debug("3 months passed after opening a finding, so sending email");
                sendThreeMonthEmail(notCompletedFinding, notCompletedFinding.getAuditType().getEmailStrategyContentOne().getMailContent());
            } else if (period.getMonths() > 0 && period.getWeeks() == 1 && period.getDays() == 0) {
                boolean sendSecondEmail = false;
                CHECK_SENDING_SECOND_EMAIL_REQUIREMENTS:
                for (FindingAction findingAction : notCompletedFinding.getFindingActions()) {
                    LocalDate userActionDate = new LocalDate(findingAction.getChangeDate());
                    LocalDate aWeekBefore = today.minusWeeks(1);
                    if (findingAction.getChangeDate() == null || userActionDate.isBefore(aWeekBefore)) {
                        sendSecondEmail = true;
                        break CHECK_SENDING_SECOND_EMAIL_REQUIREMENTS;
                    }
                }
                if (sendSecondEmail) {
                    logger.debug("3 months + 1 week passed after opening a finding, so sending second email");
                    sendThreeMonthPlusOneWeekEmail(notCompletedFinding, notCompletedFinding.getAuditType().getEmailStrategyContentTwo().getMailContent());
                }
            }
        }
    }

    private void executeEnerjisaEmailStrategy(Finding notCompletedFinding) {
        LocalDate today = new LocalDate();
        LocalDate findingOpeningDate = new LocalDate(notCompletedFinding.getAuditDate());
        Period period = new Period(findingOpeningDate, today);
        if (!findingOpeningDate.equals(today) && period.getMonths() % 3 == 0
                && period.getWeeks() == 0 && period.getDays() == 0) {
            sendThreeMonthEmail(notCompletedFinding, notCompletedFinding.getAuditType().getEmailStrategyContentThree().getMailContent());
        } else {
            LocalDate findingCompletionDate = prepareCompletionDate(notCompletedFinding);
            if (today.equals(findingCompletionDate.minusWeeks(1))) {
                sendOneWeekBeforeDeadlineEmail(notCompletedFinding, notCompletedFinding.getAuditType().getEmailStrategyContentOne());
            } else if (today.equals(findingCompletionDate.plusDays(1))) {
                sendOneDayAfterDeadlineEmail(notCompletedFinding, notCompletedFinding.getAuditType().getEmailStrategyContentTwo());
            }
        }
    }

    private LocalDate prepareCompletionDate(Finding notCompletedFinding) {
        LocalDate findingCompletionDate;
        if(notCompletedFinding.getRevised() && notCompletedFinding.getRevisedCompletionDate() != null){
            findingCompletionDate = new LocalDate(notCompletedFinding.getRevisedCompletionDate());
        }else{
            findingCompletionDate = new LocalDate(notCompletedFinding.getCompletionDate());
        }
        return findingCompletionDate;
    }

    private void sendOneDayAfterDeadlineEmail(Finding notCompletedFinding, EmailContent emailStrategyContent) {
        logger.debug("trying to send one day after deadline mail");
        Preconditions.checkNotNull(emailStrategyContent, "can not find mail content template");
        String mailTemplate = emailStrategyContent.getMailContent();
        ST stringTemplate = new ST(mailTemplate, '$', '$');
        logger.info("ST is {}", stringTemplate);
        stringTemplate.add("findingNo", notCompletedFinding.getFindingNumber());
        stringTemplate.add("findingDescription", notCompletedFinding.getDescription());
        stringTemplate.add("findingPriority", notCompletedFinding.getPriority().getDescription());
        stringTemplate.add("findingStatus", notCompletedFinding.getStatusOfFinding().getDescription());
        stringTemplate.add("businessProcess", notCompletedFinding.getBusinessSubProcessType().getDescription());
        stringTemplate.add("findingDetail",notCompletedFinding.getDetail());                
        stringTemplate.add("findingRisk",notCompletedFinding.getRisk());
        stringTemplate.add("findingRecommandation",notCompletedFinding.getRecommendation());
        stringTemplate.add("managementActionPlan",notCompletedFinding.getManagementActionPlan());
        stringTemplate.add("completionDate",new SimpleDateFormat("dd/MM/yyyy").format(notCompletedFinding.getCompletionDate()));
        stringTemplate.add("URL", auditSystemURL);
        stringTemplate.add("ownersInfo", getOwnerNameWithActionPercentage(notCompletedFinding));
        String mailBody = stringTemplate.render(new Locale("tr", "TR"));
        List<String> toAdresses = User.usersEmailAddresses(notCompletedFinding.usersOfFindingActions());
        List<String> ccAddresses = User.usersEmailAddresses(notCompletedFinding.getAuditType().getAuditTypeUpdateFollowers());
        this.sendEmail(mailBody, toAdresses, ccAddresses, notCompletedFinding.getAuditType().getEmailSubject());
    }

    private void sendThreeMonthEmail(Finding finding, String mailContent) {
        sendEmailToFindingActionOwnersAndResponsibleHeads(finding, mailContent);
    }

    private void sendThreeMonthPlusOneWeekEmail(Finding finding, String mailContent) {
        sendEmailToFindingActionOwnersAndResponsibleHeads(finding, mailContent);
    }

    private void executeOneDayBeforeEmailStrategy(Finding notCompletedFinding) {
        LocalDate today = new LocalDate();
        LocalDate findingCompletionDate = new LocalDate(notCompletedFinding.getCompletionDate());
        if (today.equals(findingCompletionDate.minusDays(1))) {
            logger.debug("trying to send one day before mail strategy");
            Preconditions.checkNotNull(notCompletedFinding.getAuditType().getEmailStrategyContentOne(), "can not find mail content template");
            String mailTemplate = notCompletedFinding.getAuditType().getEmailStrategyContentOne().getMailContent();
            ST stringTemplate = new ST(mailTemplate, '$', '$');
            logger.info("ST is {}", stringTemplate);
            stringTemplate.add("findingNo", notCompletedFinding.getFindingNumber());
            stringTemplate.add("findingDescription", notCompletedFinding.getDescription());
            stringTemplate.add("findingPriority", notCompletedFinding.getPriority().getDescription());
            stringTemplate.add("findingStatus", notCompletedFinding.getStatusOfFinding().getDescription());
            stringTemplate.add("businessProcess", notCompletedFinding.getBusinessSubProcessType().getDescription());
            stringTemplate.add("findingDetail",notCompletedFinding.getDetail());                
            stringTemplate.add("findingRisk",notCompletedFinding.getRisk());
            stringTemplate.add("findingRecommandation",notCompletedFinding.getRecommendation());
            stringTemplate.add("managementActionPlan",notCompletedFinding.getManagementActionPlan());
            stringTemplate.add("completionDate",new SimpleDateFormat("dd/MM/yyyy").format(notCompletedFinding.getCompletionDate()));
            stringTemplate.add("URL", auditSystemURL);
            stringTemplate.add("ownersInfo", getOwnerNameWithActionPercentage(notCompletedFinding));
            String mailBody = stringTemplate.render(new Locale("tr", "TR"));
            List<String> toAdresses = User.usersEmailAddresses(notCompletedFinding.usersOfFindingActions());
            List<String> ccAddresses = User.usersEmailAddresses(notCompletedFinding.getAuditType().getAuditTypeUpdateFollowers());
            this.sendEmail(mailBody, toAdresses, ccAddresses, notCompletedFinding.getAuditType().getEmailSubject());
        }
    }

    private void sendOneWeekBeforeDeadlineEmail(Finding notCompletedFinding, EmailContent emailStrategyContent) {
        logger.debug("trying to send one week before mail");
        Preconditions.checkNotNull(emailStrategyContent, "can not find mail content template");
        String mailTemplate = emailStrategyContent.getMailContent();
        ST stringTemplate = new ST(mailTemplate, '$', '$');
        logger.info("ST is {}", stringTemplate);
        stringTemplate.add("findingNo", notCompletedFinding.getFindingNumber());
        stringTemplate.add("findingDescription", notCompletedFinding.getDescription());
        stringTemplate.add("findingPriority", notCompletedFinding.getPriority().getDescription());
        stringTemplate.add("findingStatus", notCompletedFinding.getStatusOfFinding().getDescription());
        stringTemplate.add("businessProcess", notCompletedFinding.getBusinessSubProcessType().getDescription());
        stringTemplate.add("findingDetail",notCompletedFinding.getDetail());                
        stringTemplate.add("findingRisk",notCompletedFinding.getRisk());
        stringTemplate.add("findingRecommandation",notCompletedFinding.getRecommendation());
        stringTemplate.add("managementActionPlan",notCompletedFinding.getManagementActionPlan());
        stringTemplate.add("completionDate",new SimpleDateFormat("dd/MM/yyyy").format(notCompletedFinding.getCompletionDate()));
        stringTemplate.add("URL", auditSystemURL);
        stringTemplate.add("ownersInfo", getOwnerNameWithActionPercentage(notCompletedFinding));
        String mailBody = stringTemplate.render(new Locale("tr", "TR"));
        List<String> toAdresses = User.usersEmailAddresses(notCompletedFinding.usersOfFindingActions());
        List<String> ccAddresses = User.usersEmailAddresses(notCompletedFinding.getAuditType().getAuditTypeUpdateFollowers());
        this.sendEmail(mailBody, toAdresses, ccAddresses, notCompletedFinding.getAuditType().getEmailSubject());
    }

    @Override
    @Async
    @Transactional(readOnly = true)
    public void sendEmailToFindingActionOwnersAndResponsibleHeads(Finding finding, String templateName) {
        if (finding.getSendEmail()) {
            sendEmail(finding, templateName);
        }
    }

    @Override
    @Async
    @Transactional(readOnly = true)
    public void sendOpeningEmailToFindingActionOwnersAndResponsibleHeads(Finding finding, String templateName) {
        if (finding.getAuditType().isSendOpeningEmail() && finding.getVersion().equals(NumberUtils.INTEGER_ZERO)) {
            sendEmailWithoutToCC(finding, templateName);
        }
    }

    private void sendEmail(Finding finding, String templateName) {
        try {

            logger.debug("trying to send mail");
            finding = this.findingService.retrieveFinding(finding.getId());
            ST stringTemplate = new ST(templateName, '$', '$');
            logger.info("ST is {}", stringTemplate);
            stringTemplate.add("findingNo", finding.getFindingNumber());
            stringTemplate.add("findingDescription", finding.getDescription());
            stringTemplate.add("findingPriority", finding.getPriority().getDescription());
            stringTemplate.add("findingStatus", finding.getStatusOfFinding().getDescription());
            stringTemplate.add("businessProcess", finding.getBusinessSubProcessType().getDescription());
            stringTemplate.add("findingDetail",finding.getDetail());                
            stringTemplate.add("findingRisk",finding.getRisk());
            stringTemplate.add("findingRecommandation",finding.getRecommendation());
            stringTemplate.add("managementActionPlan",finding.getManagementActionPlan());
            stringTemplate.add("completionDate",new SimpleDateFormat("dd/MM/yyyy").format(finding.getCompletionDate()));
            stringTemplate.add("URL", auditSystemURL);
            stringTemplate.add("ownersInfo", getOwnerNameWithActionPercentage(finding));
            String mailBody = stringTemplate.render(new Locale("tr", "TR"));
            List<String> toAdresses = User.usersEmailAddresses(finding.usersOfFindingActions());
            List<String> ccAddresses = prepareCCAddressesOfFinding(finding);
            this.sendEmail(mailBody, toAdresses, ccAddresses, finding.getAuditType().getEmailSubject());
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("can not send email {} {}", ex.getMessage(), finding);
        }
    }

    private void sendEmailWithoutToCC(Finding finding, String templateName) {
        try {

            logger.debug("trying to send mail");
            finding = this.findingService.retrieveFinding(finding.getId());
            ST stringTemplate = new ST(templateName, '$', '$');
            logger.info("ST is {}", stringTemplate);
            stringTemplate.add("findingNo", finding.getFindingNumber());
            stringTemplate.add("findingDescription", finding.getDescription());
            stringTemplate.add("findingPriority", finding.getPriority().getDescription());
            stringTemplate.add("findingStatus", finding.getStatusOfFinding().getDescription());
            stringTemplate.add("businessProcess", finding.getBusinessSubProcessType().getDescription());
            stringTemplate.add("findingDetail",finding.getDetail());                
            stringTemplate.add("findingRisk",finding.getRisk());
            stringTemplate.add("findingRecommandation",finding.getRecommendation());
            stringTemplate.add("managementActionPlan",finding.getManagementActionPlan());
            stringTemplate.add("completionDate",new SimpleDateFormat("dd/MM/yyyy").format(finding.getCompletionDate()));
            stringTemplate.add("URL", auditSystemURL);
            stringTemplate.add("ownersInfo", getOwnerNameWithActionPercentage(finding));
            String mailBody = stringTemplate.render(new Locale("tr", "TR"));
            List<String> toAdresses = User.usersEmailAddresses(finding.usersOfFindingActions());
            this.sendEmail(mailBody, toAdresses, null, finding.getAuditType().getEmailSubject());
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("can not send email {} {}", ex.getMessage(), finding);
        }
    }

    private List<String> prepareCCAddressesOfFinding(Finding finding) {
        List<String> ccAddresses = User.usersEmailAddresses(finding.getBusinessSubProcessType().getResponsibleHeads());
        if (finding.getPriority() == Priority.VERY_HIGH || finding.getPriority() == Priority.HIGH || finding.getStatusOfFinding() == Status.DELAYED) {
            ccAddresses.addAll(User.usersEmailAddresses(finding.getBusinessProcessType().getResponsibleHeads()));
        }
        return ccAddresses;
    }

    private void sendEmail(String mailBody, List<String> toAddress, List<String> ccAddresses, String mailSubject) {
        try {
            if (!enableEmails){
                logger.debug("emails are disabled in configuration");
                return;
            } else {
                logger.debug("trying to send mail");
                MimeMessage mimeMessage = this.javaMailSender.createMimeMessage();
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "ISO-8859-9");
                message.setFrom("afus@cimsa.com.tr");
                message.setTo(toAddress.toArray(new String[0]));
                if (ccAddresses != null) {
                    message.setCc(ccAddresses.toArray(new String[0]));
                }
                message.setBcc("d.pinar@cimsa.com.tr");
                message.setSubject(mailSubject);
                message.setText(mailBody);

                this.javaMailSender.send(mimeMessage);

                logger.debug("mail sent successfully");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("can not send email {}", ex.getMessage());
        }
    }

    @Override
    @Async
    @Transactional(readOnly = true)
    public void sendFindingActionUpdateEmail(FindingAction findingAction) {
        try {
            Finding finding = this.findingService.retrieveFinding(findingAction.getFinding().getId());
            if (CollectionUtils.isNotEmpty(finding.getAuditType().getAuditTypeUpdateFollowers())
                    && finding.getAuditType().getUpdateEmailContent() != null) {
                logger.debug("trying to send finding action update mail");
                List<String> toAdresses = User.usersEmailAddresses(finding.getAuditType().getAuditTypeUpdateFollowers());
                String mailTemplate = finding.getAuditType().getUpdateEmailContent().getMailContent();
                ST stringTemplate = new ST(mailTemplate, '$', '$');
                logger.info("ST is {}", stringTemplate);
                stringTemplate.add("findingNo", finding.getFindingNumber());
                stringTemplate.add("userName", findingAction.getActionOwner().getNameSurname());
                stringTemplate.add("findingDescription", finding.getDescription());
                stringTemplate.add("findingPriority", finding.getPriority().getDescription());
                stringTemplate.add("findingStatus", finding.getStatusOfFinding().getDescription());
                stringTemplate.add("businessProcess", finding.getBusinessSubProcessType().getDescription());
                stringTemplate.add("findingDetail",finding.getDetail());                
                stringTemplate.add("findingRisk",finding.getRisk());
                stringTemplate.add("findingRecommandation",finding.getRecommendation());
                stringTemplate.add("managementActionPlan",finding.getManagementActionPlan());
                stringTemplate.add("completionDate",new SimpleDateFormat("dd/MM/yyyy").format(finding.getCompletionDate()));
                
                stringTemplate.add("URL", auditSystemURL);
                stringTemplate.add("ownersInfo", getOwnerNameWithActionPercentage(finding));
                String mailBody = stringTemplate.render(new Locale("tr", "TR"));
                this.sendEmail(mailBody, toAdresses, Lists.<String>newArrayList(), finding.getAuditType().getEmailSubject());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("can not send finding action update email {}", ex.getMessage());
        }
    }
    
    public String[] getOwnerNameWithActionPercentage(Finding finding)
    {
        List<String> ownersInfo = Lists.newLinkedList();
        for (FindingAction findingAction : finding.getFindingActions())
        {
            StringBuilder sb = new StringBuilder();
            sb.append(StringUtils.defaultString(findingAction.getActionOwner().getNameSurname(), StringUtils.EMPTY));
            sb.append(" - ");
            sb.append("% " + findingAction.getCompletionPercentage());
            ownersInfo.add(sb.toString());
        }
        return ownersInfo.toArray(new String[ownersInfo.size()]);
    }

    @Resource(name = "mailSender")
    public void setJavaMailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Resource(name = "templateMessage")
    public void setTemplateMailMessage(SimpleMailMessage templateMailMessage) {
        this.templateMailMessage = templateMailMessage;
    }

    @Resource(name = "findingService")
    public void setFindingService(FindingService findingService) {
        this.findingService = findingService;
    }

    @Resource(name = "auditSystemURL")
    public void setAuditSystemURL(String auditSystemURL) {
        this.auditSystemURL = auditSystemURL;
    }

    @Resource(name = "enableEmails")
    public void setEnableEmails(Boolean enableEmails) {
        this.enableEmails = enableEmails;
    }
    
    @Resource(name = "batchJobEnabled")
    public void setBatchJobEnabled(Boolean batchJobEnabled) {
        this.batchJobEnabled = batchJobEnabled;
    }
}

