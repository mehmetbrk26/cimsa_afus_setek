package com.tr.aksigorta.denetimtaleptakip.services.impl;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.tr.aksigorta.denetimtaleptakip.controller.admin.SearchParameters;
import com.tr.aksigorta.denetimtaleptakip.dao.FindingDAO;
import com.tr.aksigorta.denetimtaleptakip.dao.GenericDAO;
import com.tr.aksigorta.denetimtaleptakip.model.*;
import com.tr.aksigorta.denetimtaleptakip.services.FindingService;
import com.tr.aksigorta.denetimtaleptakip.services.LoggingServis;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:04 PM
 */
@Service("findingService")
public class FindingServiceImpl extends BaseServiceImpl implements FindingService
{

    private FindingDAO findingDAO;

    private LoggingServis loggingServis;

    @Override
    public List<AuditType> retrieveAuditTypes()
    {
        return findingDAO.retrieveAllAuditTypes();
    }
    
    @Override
    public List<Company> retrieveCompanies()
    {
        return findingDAO.retrieveAllCompanies();
    }

    @Override
    public AuditType retrieveAuditType(String description) {
        return this.findingDAO.retrieveAuditType(description);
    }
    
    @Override
    public Company retrieveCompany(String description) {
        return this.findingDAO.retrieveCompany(description);
    }

    @Override
    public List<AuditType> retrieveAllActiveAuditTypes()
    {
        return  this.findingDAO.retrieveAllActiveAuditTypes();
    }
    
    @Override
    public List<Company> retrieveAllActiveCompanies()
    {
        return  this.findingDAO.retrieveAllActiveCompanies();
    }

    @Override
    public List<BusinessProcessType> retrieveBusinessProcesses()
    {
        return findingDAO.findAll(BusinessProcessType.class);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Finding mergeFinding(Finding finding)
    {
        return this.findingDAO.merge(finding);
    }

    @Override
    public List<Finding> retrieveAllFindings()
    {
        return this.findingDAO.retrieveAllFindings();
    }

    @Override
    public List<FindingAction> retrieveUserFindingActions(User user)
    {
        return this.findingDAO.retrieveUserFindingActions(user);
    }

    @Override
    @Transactional
    public void mergeFindingAction(FindingAction selectedFindingAction)
    {
        selectedFindingAction = this.findingDAO.merge(selectedFindingAction);
        this.findingDAO.flushSessionChanges();

        if(selectedFindingAction.getCompletionPercentage() == 100){
            selectedFindingAction.getFinding().setCompleted(Boolean.TRUE);
        }else{
            selectedFindingAction.getFinding().setCompleted(Boolean.FALSE);
        }
        if(selectedFindingAction.getRevisedCompletionDate() != null){
            selectedFindingAction.getFinding().setRevisedCompletionDate(selectedFindingAction.getRevisedCompletionDate());
            selectedFindingAction.getFinding().setRevised(false);
        }
        selectedFindingAction.getFinding().setConfirmed(Boolean.FALSE);
        
        this.findingDAO.merge(selectedFindingAction);
    }

    @Override
    @Transactional
    public void mergeFindingResponsibleAction(FindingResponsibleAction selectedFindingResponsibleAction)
    {
        selectedFindingResponsibleAction = this.findingDAO.merge(selectedFindingResponsibleAction);
        this.findingDAO.flushSessionChanges();
        if(selectedFindingResponsibleAction.getCompletionPercentage() == 100){
            selectedFindingResponsibleAction.getFinding().setCompleted(Boolean.TRUE);
        }else{
            selectedFindingResponsibleAction.getFinding().setCompleted(Boolean.FALSE);
        }
        if(selectedFindingResponsibleAction.getRevisedCompletionDate() != null){
            selectedFindingResponsibleAction.getFinding().setRevisedCompletionDate(selectedFindingResponsibleAction.getRevisedCompletionDate());
            selectedFindingResponsibleAction.getFinding().setRevised(true);
        }
        selectedFindingResponsibleAction.getFinding().setConfirmed(Boolean.TRUE);
        this.findingDAO.merge(selectedFindingResponsibleAction);
    }
    
    @Override
    public Finding retrieveFinding(Integer findingIdToUpdate)
    {
        return this.findingDAO.findById(Finding.class, findingIdToUpdate);
    }

    @Override
    public Finding retrieveFinding(String findingNumber) {
        return this.findingDAO.retrieveFinding(findingNumber);
    }

    @Override
    public List<BusinessSubProcessType> retrieveBusinessSubProcesses()
    {
        return this.findingDAO.retrieveBusinessSubProcesses();
    }

    @Override
    public List<Finding> searchFindings(SearchParameters searchParameters, User user)
    {
        List<Finding> findings = this.findingDAO.searchFindings(searchParameters);
        findings = searchParameters.filterWithStatus(findings);
        return searchParameters.filterWithUserRole(findings, user);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Finding> retrieveNotCompletedFindings()
    {
        SearchParameters searchParameters = new SearchParameters();
        searchParameters.setStatus(Status.DELAYED);
        return this.findingDAO.searchFindings(searchParameters);
    }

    @Override
    public FindingAction retrieveFindingActionWithHistory(Integer findingActionId)
    {
        return this.findingDAO.retrieveFindingActionWithHistory(findingActionId);
    }

    @Override
    public List<FindingAction> retrieveHistoryOfFindingAction(Integer findingActionId)
    {
        return this.findingDAO.retrieveHistoryOfFindingAction(findingActionId);
    }

    @Override
    public List<FindingResponsibleAction> retrieveHistoryOfFindingResponsibleAction(Integer findingResponsibleActionId) {
        return this.findingDAO.retrieveHistoryOfFindingResponsibleAction(findingResponsibleActionId);
    }

    @Override
    @Transactional
    public AuditType mergeAuditType(AuditType auditType, List<BusinessProcessType> businessProcessTypes, List<User> followers)
    {
        if(auditType.getId() == null)
        {
            auditType.getBusinessProcessTypes().addAll(businessProcessTypes);
            auditType.getAuditTypeUpdateFollowers().addAll(followers);
        }
        else
        {
            auditType.getBusinessProcessTypes().addAll(businessProcessTypes);
            auditType.getAuditTypeUpdateFollowers().addAll(followers);
            for(Iterator iterator = auditType.getBusinessProcessTypes().iterator(); iterator.hasNext();)
            {
                if(!businessProcessTypes.contains(iterator.next()))
                {
                    iterator.remove();
                }
            }
            for(Iterator iterator = auditType.getAuditTypeUpdateFollowers().iterator(); iterator.hasNext();)
            {
                if(!followers.contains(iterator.next()))
                {
                    iterator.remove();
                }
            }
        }
        return this.findingDAO.merge(auditType);
    }

    @Override
    @Transactional
    public BusinessProcessType mergeBusinessType(BusinessProcessType businessProcessType, List<User> responsibleHeads)
    {
        if(businessProcessType.getId() == null)
        {
            businessProcessType.getResponsibleHeads().addAll(responsibleHeads);
        }
        else
        {
            businessProcessType.getResponsibleHeads().addAll(responsibleHeads);
            for(Iterator iterator = businessProcessType.getResponsibleHeads().iterator(); iterator.hasNext();)
            {
                if(!responsibleHeads.contains(iterator.next()))
                {
                    iterator.remove();
                }
            }
        }
        return this.findingDAO.merge(businessProcessType);
    }

    @Override
    public List<Finding> retrieveUserResponsibleDepartmentsFindingActions(User user)
    {
        List<Finding> findings = Lists.newLinkedList();
        if(CollectionUtils.isNotEmpty(user.getResponsibleBusinessTypes()))
        {
            findings.addAll(findingDAO.retrieveUserResponsibleBusinessTypeFindings(user.getResponsibleBusinessTypes()));
        }
        if(CollectionUtils.isNotEmpty(user.getResponsibleBusinessSubTypes()))
        {
            findings.addAll(findingDAO.retrieveUserResponsibleBusinessSubTypeFindings(user.getResponsibleBusinessSubTypes()));
        }
        return findings;
    }

    @Override
    public List<EmailSendingStrategy> retrieveAllEmailStrategies()
    {
        return getDAO().findAll(EmailSendingStrategy.class);
    }

    @Override
    public List<EmailContent> retrieveAllEmailContents()
    {
        return getDAO().findAll(EmailContent.class);
    }

    @Override
    @Transactional
    public void reassignFinding(Finding finding, List<User> oldActionOwners, List<User> selectedOldActionOwners, User selectedNewActionOwner)
    {
        FindingAction newActionOwnerFindingAction = defineNewActionOwnerFindingAction(finding, oldActionOwners, selectedNewActionOwner);
        this.transferOldUserActionsToNewUser(finding, selectedOldActionOwners, newActionOwnerFindingAction);
        if( selectedOldActionOwners.contains(newActionOwnerFindingAction.getActionOwner()))
        {
        	selectedOldActionOwners.remove(newActionOwnerFindingAction.getActionOwner());
        }
        this.findingDAO.deleteFindingActionsOfUsers(finding, selectedOldActionOwners);
    }

    @Override
    public BusinessProcessType retrieveBusinessProcessType(String description) {
        return this.findingDAO.retrieveBusinessProcessType(description);
    }

    @Override
    public List<Finding> retrieveAllFindings(int first, int pageSize, String sortField, SortOrder sortOrder) {
        return this.findingDAO.retrieveAllFindings(first, pageSize, sortField, sortOrder);
    }

    @Override
    public Long retrieveAllFindingsCount() {
        return this.findingDAO.retrieveAllFindingsCount();
    }

    @Override
    public Long retrieveUserResponsibleDepartmentsFindingActionsCount(User user) {
        return this.findingDAO.retrieveUserResponsibleDepartmentsFindingActionsCount(user);
    }

    @Override
    public List<Finding> retrieveUserResponsibleDepartmentsFindingActions(int first, int pageSize, String sortField, SortOrder sortOrder, User user) {
        return this.findingDAO.retrieveUserResponsibleDepartmentsFindingActions(first, pageSize, sortField, sortOrder, user);
    }

    @Override
    public Long retrieveUserFindingActionsCount(User user) {
        return this.findingDAO.retrieveUserFindingActionsCount(user);
    }

    @Override
    public List<FindingAction> retrieveUserFindingActions(int first, int pageSize, String sortField, SortOrder sortOrder, User user) {
        return this.findingDAO.retrieveUserFindingActions(first, pageSize, sortField, sortOrder, user);
    }

    private FindingAction defineNewActionOwnerFindingAction(Finding finding, List<User> oldActionOwners, User selectedNewActionOwner)
    {
        FindingAction newActionOwnerFindingAction = null;
        if(oldActionOwners.contains(selectedNewActionOwner))
        {
            newActionOwnerFindingAction = finding.findingActionOfUser(selectedNewActionOwner);
        }
        else
        {
            newActionOwnerFindingAction = new FindingAction();
            newActionOwnerFindingAction.setFinding(finding);
            newActionOwnerFindingAction.setActionOwner(selectedNewActionOwner);
            newActionOwnerFindingAction.setReadOnly(false);
            newActionOwnerFindingAction.setChangeDate(new Date());
        }
        return newActionOwnerFindingAction;
    }

    private void transferOldUserActionsToNewUser(Finding finding, List<User> oldActionOwners, FindingAction newActionOwnerFindingAction)
    {
        if (CollectionUtils.isEmpty(oldActionOwners))
        {
            this.findingDAO.merge(newActionOwnerFindingAction);
        }
        else
        {
            Joiner joiner = Joiner.on(" ").skipNulls();
            for (User oldActionOwner : oldActionOwners)
            {
                FindingAction oldUserFindingAction = finding.findingActionOfUser(oldActionOwner);
                StringBuilder oldActionOwnerIdentifier = new StringBuilder("");
                oldActionOwnerIdentifier.append(oldActionOwner.getNameSurname());
                oldActionOwnerIdentifier.append(":");
                if(StringUtils.isNotBlank(oldUserFindingAction.getActionTaken()))
                {
                    newActionOwnerFindingAction.setActionTaken(joiner.join(newActionOwnerFindingAction.getActionTaken(), "(", oldActionOwnerIdentifier.toString(), oldUserFindingAction.getActionTaken(), ")"));
                }
                if(StringUtils.isNotBlank(oldUserFindingAction.getExplanationForDelay()))
                {
                    newActionOwnerFindingAction.setExplanationForDelay(joiner.join(newActionOwnerFindingAction.getExplanationForDelay(), "(", oldActionOwnerIdentifier.toString(), oldUserFindingAction.getExplanationForDelay(), ")"));
                }
                adjustCompletionPercentage(newActionOwnerFindingAction, oldUserFindingAction);
                adjustRevisedCompletionDate(newActionOwnerFindingAction, oldUserFindingAction);
            }
            newActionOwnerFindingAction = this.loggingServis.mergeLog(newActionOwnerFindingAction);
        }
    }

    private void adjustCompletionPercentage(FindingAction newActionOwnerFindingAction, FindingAction oldUserFindingAction)
    {
        int completionPercentage = newActionOwnerFindingAction.getCompletionPercentage();
        if (completionPercentage < oldUserFindingAction.getCompletionPercentage())
        {
            completionPercentage = oldUserFindingAction.getCompletionPercentage();
        }
        newActionOwnerFindingAction.setCompletionPercentage(completionPercentage);
    }

    private void adjustRevisedCompletionDate(FindingAction newActionOwnerFindingAction, FindingAction oldUserFindingAction)
    {
        Date revisedCompletionDate = newActionOwnerFindingAction.getRevisedCompletionDate();
        if(revisedCompletionDate != null)
        {
            Date oldUserRevisedCompletionDate = oldUserFindingAction.getRevisedCompletionDate();
            if(oldUserRevisedCompletionDate != null && oldUserRevisedCompletionDate.after(revisedCompletionDate))
            {
                revisedCompletionDate = oldUserRevisedCompletionDate;
            }
        }
        else
        {
            revisedCompletionDate =  oldUserFindingAction.getRevisedCompletionDate();
        }
        newActionOwnerFindingAction.setRevisedCompletionDate(revisedCompletionDate);
    }
    
    @Override
    public AuditType findByProperty(Class<?> clazz, String propertyName, String value)
    {
        return findingDAO.findByProperty(clazz, propertyName, value);
    }

    @Override
    protected GenericDAO getDAO()
    {
        return findingDAO;
    }

    @Resource(name = "findingDAO")
    public void setFindingDAO(FindingDAO findingDAO)
    {
        this.findingDAO = findingDAO;
    }

    @Resource(name = "loggingServis")
    public void setLoggingServis(LoggingServis loggingServis)
    {
        this.loggingServis = loggingServis;
    }

	@Override
	public void deleteFinding(Finding finding) {
		findingDAO.deleteFinding(finding);
	}
}
