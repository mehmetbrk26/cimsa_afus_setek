package com.tr.aksigorta.denetimtaleptakip.services;

import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import com.tr.aksigorta.denetimtaleptakip.model.Finding;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:04 PM
 */
public interface SummaryReportExcelService extends BaseService
{
    public Workbook prepareExcelReportOfTheFindings(List<Finding> findings);
}
