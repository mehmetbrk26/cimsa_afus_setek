package com.tr.aksigorta.denetimtaleptakip.services;

import com.tr.aksigorta.denetimtaleptakip.model.Finding;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:04 PM
 */
public interface FindingExcelService extends BaseService
{
    public Workbook prepareExcelReportOfTheFindings(List<Finding> findings);

    public Workbook prepareExcelTemplate();


}
