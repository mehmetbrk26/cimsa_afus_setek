package com.tr.aksigorta.denetimtaleptakip.services.impl;

import com.tr.aksigorta.denetimtaleptakip.dao.FindingDAO;
import com.tr.aksigorta.denetimtaleptakip.dao.GenericDAO;
import com.tr.aksigorta.denetimtaleptakip.model.*;
import com.tr.aksigorta.denetimtaleptakip.services.FindingExcelService;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:04 PM
 */
@Service("findingExcelService")
public class FindingExcelServiceImpl extends BaseServiceImpl implements FindingExcelService {
    private FindingDAO findingDAO;

    @Override
    public Workbook prepareExcelReportOfTheFindings(List<Finding> findings) {
        Workbook workbook = new HSSFWorkbook();
        Map<String, CellStyle> styles = createStyles(workbook);
        Sheet sheet = workbook.createSheet("Findings");
        int baslikSayisi = baslikOlustur(sheet, styles);
        int i = 1;
        for (Finding finding : findings) {
            Row row = sheet.createRow(i);

            int baslikNo = 0;

            Cell cell = row.createCell(baslikNo++);
            Company company = ((Finding) finding).getCompany();
            String companyName = company != null ? company.getDescription() : "";
            cell.setCellValue(companyName);
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getAuditType().getDescription());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getBusinessProcessType().getDescription());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getBusinessSubProcessType().getDescription());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getFindingNumber());
            cell.setCellStyle(styles.get("normalStyle"));
            
            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getFindingYear() != null ? new LocalDate(finding.getFindingYear()).toString("yyyy") : StringUtils.EMPTY);
            cell.setCellStyle(styles.get("normalStyle"));
            
            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getReportName());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getDescription());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getPriority().getDescription());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getDetail());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getRisk());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getRecommendation());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getManagementActionPlan());
            cell.setCellStyle(styles.get("normalStyle"));

            StringBuilder actionOwnersStringBuilder = new StringBuilder("");
            StringBuilder actionTakenStringBuilder = new StringBuilder("");
            StringBuilder actionTakenDateStringBuilder = new StringBuilder("");
            StringBuilder explanationForDelayStringBuilder = new StringBuilder("");
            StringBuilder revisedCompletionDateStringBuilder = new StringBuilder("");
            for (FindingAction findingAction : finding.findingActionsAsList()) {
                actionOwnersStringBuilder.append(findingAction.getActionOwner().getNameSurname());
                actionOwnersStringBuilder.append("\n");

                if (StringUtils.isNotBlank(findingAction.getActionTaken())) {
                    actionTakenStringBuilder.append(findingAction.getActionTaken());
                    actionTakenStringBuilder.append("(");
                    actionTakenStringBuilder.append(findingAction.getActionOwner().getNameSurname());
                    actionTakenStringBuilder.append(")");
                    actionTakenStringBuilder.append("\n");
                }

                if (findingAction.getChangeDate() != null) {
                    actionTakenDateStringBuilder.append(new LocalDate(findingAction.getChangeDate()).toString("dd/MM/yyyy"));
                    actionTakenDateStringBuilder.append("(");
                    actionTakenDateStringBuilder.append(findingAction.getActionOwner().getNameSurname());
                    actionTakenDateStringBuilder.append(")");
                    actionTakenDateStringBuilder.append("\n");
                }

                if (StringUtils.isNotBlank(findingAction.getExplanationForDelay())) {
                    explanationForDelayStringBuilder.append(findingAction.getExplanationForDelay());
                    explanationForDelayStringBuilder.append("(");
                    explanationForDelayStringBuilder.append(findingAction.getActionOwner().getNameSurname());
                    explanationForDelayStringBuilder.append(")");
                    explanationForDelayStringBuilder.append("\n");
                }

                if (findingAction.getRevisedCompletionDate() != null) {
                    revisedCompletionDateStringBuilder.append(new LocalDate(findingAction.getRevisedCompletionDate()).toString("dd/MM/yyyy"));
                    revisedCompletionDateStringBuilder.append("(");
                    revisedCompletionDateStringBuilder.append(findingAction.getActionOwner().getNameSurname());
                    revisedCompletionDateStringBuilder.append(")");
                    revisedCompletionDateStringBuilder.append("\n");
                }
            }
            cell = row.createCell(baslikNo++);
            cell.setCellValue(actionOwnersStringBuilder.toString());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getCompletionDate());
            cell.setCellStyle(styles.get("dateStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(actionTakenStringBuilder.toString());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(actionTakenDateStringBuilder.toString());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(explanationForDelayStringBuilder.toString());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(revisedCompletionDateStringBuilder.toString());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getSendEmail());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getReasonToNotSendEmail() != null ? finding.getReasonToNotSendEmail().getDescription() : StringUtils.EMPTY);
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getCompleted());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getStatusOfFinding().getDescription());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getCompletionPercentage() + " %");
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getActionTakenFromActions());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(baslikNo++);
            cell.setCellValue(finding.getActionTakenFromResponsibleActions());
            cell.setCellStyle(styles.get("normalStyle"));

            i++;
        }
        for (i = baslikSayisi; i >= 0; i--)
            sheet.autoSizeColumn(i);
        return workbook;
    }

    @Override
    public Workbook prepareExcelTemplate() {
        Workbook workbook = new HSSFWorkbook();
        Map<String, CellStyle> styles = createStyles(workbook);
        Sheet sheet = workbook.createSheet("Findings");
        int baslikSayisi = prepareHeaderForTemplate(sheet, styles, workbook);

        prepareRowStylesForTemplate(styles, sheet);
        prepareDataValidation(styles, sheet, workbook);
        prepareColumnWidth(sheet, baslikSayisi);

        return workbook;
    }

    private void prepareColumnWidth(Sheet sheet, int baslikSayisi) {
        for (int i = 0; i < baslikSayisi; i++){
            sheet.autoSizeColumn(i);
        }
        sheet.setColumnWidth(9, 6000);
        sheet.setColumnWidth(10, 6000);
        sheet.setColumnWidth(11, 6000);
        sheet.setColumnWidth(15, 6000);
        sheet.setColumnWidth(16, 6000);
    }

    private void prepareRowStylesForTemplate(Map<String, CellStyle> styles, Sheet sheet) {
        for (int i = 1; i <= 50; i++) {
            Row row = sheet.createRow(i);

            for (int j = 0; j <=14 ; j++) {
                Cell cell = row.createCell(0);
                cell.setCellValue("");
                cell.setCellStyle(styles.get(j == 14 ? "dateStyle" : "normalStyle"));
            }
        }
    }

    private void prepareDataValidation(Map<String, CellStyle> styles, Sheet sheet, Workbook workbook) {
        prepareCompanyDataValidation(workbook, sheet, styles);
        prepareAuditTypeDataValidation(workbook, sheet, styles);
        prepareBusinessProcessTypeDataValidation(workbook, sheet, styles);
        prepareBusinessSubProcessTypeDataValidation(workbook, sheet, styles);
        preparePriorityDataValidation(sheet);
    }

    private void prepareCompanyDataValidation(Workbook workbook, Sheet mainSheet, Map<String, CellStyle> styles) {
        Sheet hidden = workbook.createSheet("hiddenSheetCompany");
        workbook.setSheetHidden(1, true);
        final List<Company> companies = findingDAO.retrieveAllCompanies();
        if (companies != null && companies.size() > 0) {
            int i = 0;
            for (Company company : companies) {
                Row row = hidden.createRow(i++);
                Cell cell = row.createCell(0);
                cell.setCellValue(company.getDescription());
            }
            Name namedCell = workbook.createName();
            namedCell.setNameName("company");
            namedCell.setRefersToFormula("hiddenSheetCompany!$A$1:$A$" + companies.size());
            DVConstraint constraint = DVConstraint.createFormulaListConstraint("company");
            CellRangeAddressList addressList = new CellRangeAddressList(1, 50, 0, 0);
            HSSFDataValidation dataValidation = new HSSFDataValidation(addressList, constraint);
            dataValidation.setSuppressDropDownArrow(false);
            dataValidation.setErrorStyle(DataValidation.ErrorStyle.STOP);
            dataValidation.createErrorBox("Error", "Please enter valid Company value.");
            mainSheet.addValidationData(dataValidation);
        }
    }

    private void prepareAuditTypeDataValidation(Workbook workbook, Sheet mainSheet, Map<String, CellStyle> styles) {
        Sheet hidden = workbook.createSheet("hiddenSheetAuditType");
        workbook.setSheetHidden(2, true);
        final List<AuditType> auditTypes = findingDAO.retrieveAllAuditTypes();
        if (auditTypes != null && auditTypes.size() > 0) {
            int i = 0;
            for (AuditType auditType : auditTypes) {
                Row row = hidden.createRow(i++);
                Cell cell = row.createCell(0);
                cell.setCellValue(auditType.getDescription());
            }
            Name namedCell = workbook.createName();
            namedCell.setNameName("auditType");
            namedCell.setRefersToFormula("hiddenSheetAuditType!$A$1:$A$" + auditTypes.size());
            DVConstraint constraint = DVConstraint.createFormulaListConstraint("auditType");
            CellRangeAddressList addressList = new CellRangeAddressList(1, 50, 1, 1);
            HSSFDataValidation dataValidation = new HSSFDataValidation(addressList, constraint);
            dataValidation.setSuppressDropDownArrow(false);
            dataValidation.setErrorStyle(DataValidation.ErrorStyle.STOP);
            dataValidation.createErrorBox("Error", "Please enter valid Audit Type value.");
            mainSheet.addValidationData(dataValidation);
        }
    }

    private void prepareBusinessProcessTypeDataValidation(Workbook workbook, Sheet mainSheet, Map<String, CellStyle> styles) {
        Sheet hidden = workbook.createSheet("hiddenSheetBusinessProcessType");
        workbook.setSheetHidden(3, true);
        final List<BusinessProcessType> businessProcessTypes = findingDAO.retrieveBusinessProcesses();
        if (businessProcessTypes != null && businessProcessTypes.size() > 0) {
            int i = 0;
            for (BusinessProcessType businessProcessType : businessProcessTypes) {
                Row row = hidden.createRow(i++);
                Cell cell = row.createCell(0);
                cell.setCellValue(businessProcessType.getDescription());
            }
            Name namedCell = workbook.createName();
            namedCell.setNameName("businessProcessType");
            namedCell.setRefersToFormula("hiddenSheetBusinessProcessType!$A$1:$A$" + businessProcessTypes.size());
            DVConstraint constraint = DVConstraint.createFormulaListConstraint("businessProcessType");
            CellRangeAddressList addressList = new CellRangeAddressList(1, 50, 2, 2);
            HSSFDataValidation dataValidation = new HSSFDataValidation(addressList, constraint);
            dataValidation.setSuppressDropDownArrow(false);
            dataValidation.setErrorStyle(DataValidation.ErrorStyle.STOP);
            dataValidation.createErrorBox("Error", "Please enter valid Business Process Type value.");
            mainSheet.addValidationData(dataValidation);
        }
    }

    private void prepareBusinessSubProcessTypeDataValidation(Workbook workbook, Sheet mainSheet, Map<String, CellStyle> styles) {
        Sheet hidden = workbook.createSheet("hiddenSheetBusinessSubProcessType");
        workbook.setSheetHidden(4, true);
        final List<BusinessSubProcessType> businessSubProcessTypes = findingDAO.retrieveBusinessSubProcesses();
        if (businessSubProcessTypes != null && businessSubProcessTypes.size() > 0) {
            int i = 0;
            for (BusinessSubProcessType businessSubProcessType : businessSubProcessTypes) {
                Row row = hidden.createRow(i++);
                Cell cell = row.createCell(0);
                cell.setCellValue(businessSubProcessType.getDescription());
            }
            Name namedCell = workbook.createName();
            namedCell.setNameName("businessSubProcessType");
            namedCell.setRefersToFormula("hiddenSheetBusinessSubProcessType!$A$1:$A$" + businessSubProcessTypes.size());
            DVConstraint constraint = DVConstraint.createFormulaListConstraint("businessSubProcessType");
            CellRangeAddressList addressList = new CellRangeAddressList(1, 50, 3, 3);
            HSSFDataValidation dataValidation = new HSSFDataValidation(addressList, constraint);
            dataValidation.setSuppressDropDownArrow(false);
            dataValidation.setErrorStyle(DataValidation.ErrorStyle.STOP);
            dataValidation.createErrorBox("Error", "Please enter valid Business Sub Process Type value.");
            mainSheet.addValidationData(dataValidation);
        }
    }

    private void preparePriorityDataValidation(Sheet sheet) {
        CellRangeAddressList addressList = new CellRangeAddressList(1, 50, 8, 8);
        String[] explicitListValues = new String[Priority.values().length];
        int i = 0;
        for (Priority priority : Priority.values()) {
            explicitListValues[i++] = priority.getDescription();
        }
        DVConstraint dvConstraint = DVConstraint.createExplicitListConstraint(explicitListValues);
        DataValidation dataValidation = new HSSFDataValidation(addressList, dvConstraint);
        dataValidation.setSuppressDropDownArrow(false);
        dataValidation.setErrorStyle(DataValidation.ErrorStyle.STOP);
        dataValidation.createErrorBox("Error", "Please enter valid Priority value.");
        sheet.addValidationData(dataValidation);
    }

    private Map<String, CellStyle> createStyles(Workbook workbook) {
        Map<String, CellStyle> styles = new HashMap<String, CellStyle>();
        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        font.setFontName("Arial");
        font.setColor(IndexedColors.BLACK.getIndex());

        DataFormat dateFormat = workbook.createDataFormat();

        CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        style.setWrapText(true);
        styles.put("normalStyle", style);

        style = workbook.createCellStyle();
        style.setFont(font);
        style.setDataFormat(dateFormat.getFormat("dd/MM/yyyy"));
        styles.put("dateStyle", style);

        CellStyle headerStyle = workbook.createCellStyle();

        headerStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        font.setFontName("Arial");
        font.setColor(IndexedColors.BLACK.getIndex());
        headerStyle.setFont(font);
        styles.put("criteriaHeaderStyle", headerStyle);
        headerStyle.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
        styles.put("headerStyle", headerStyle);
        return styles;
    }

    private int baslikOlustur(Sheet sheet, Map<String, CellStyle> styles) {
        int baslikNo = 0;
        Row row = sheet.createRow(0);

        Cell cell = row.createCell(baslikNo++);
        cell.setCellValue("Company");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Audit Type");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Main Process");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Sub Process");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Finding No");
        cell.setCellStyle(styles.get("headerStyle"));
        
        cell = row.createCell(baslikNo++);
        cell.setCellValue("Finding Year");
        cell.setCellStyle(styles.get("headerStyle"));
        
        cell = row.createCell(baslikNo++);
        cell.setCellValue("Report Name");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Finding Title");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Priority");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Detail");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Risk");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Recommendation");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Management Action Plan");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Action Owner");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Completion Date");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Action Taken");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Action Taken Date");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Explanation for Delay");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Revised Completion Date");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Do you want to send warning mails?");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Reason to not send email");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Completed");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Status");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Completion Percentage");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Last Action");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Last Confirmation");
        cell.setCellStyle(styles.get("headerStyle"));

        return baslikNo;
    }

    private int prepareHeaderForTemplate(Sheet sheet, Map<String, CellStyle> styles, Workbook workbook) {
        int baslikNo = 0;
        Row row = sheet.createRow(0);

        Cell cell = row.createCell(baslikNo++);
        cell.setCellValue("Company");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Audit Type");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Main Process");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Sub Process");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Finding No");
        cell.setCellStyle(styles.get("headerStyle"));
        
        cell = row.createCell(baslikNo++);
        cell.setCellValue("Finding Year");
        cell.setCellStyle(styles.get("headerStyle"));
        
        cell = row.createCell(baslikNo++);
        cell.setCellValue("Report Name");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Finding Title");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Priority");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Detail");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Risk");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Recommendation");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Management Action Plan");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Action Owner");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Completion Date");
        cell.setCellStyle(styles.get("headerStyle"));
        cell.setCellComment(getComment(sheet, workbook, row, cell, "Format: dd/MM/yyyy"));

        //baslikSayisi = (int) row.getLastCellNum();
        return baslikNo;
    }

    private Comment getComment(Sheet sheet, Workbook workbook, Row row, Cell cell, String commentString) {
        Drawing drawing = sheet.createDrawingPatriarch();
        CreationHelper factory = workbook.getCreationHelper();
        ClientAnchor anchor = factory.createClientAnchor();

        anchor.setCol1(cell.getColumnIndex());
        anchor.setCol2(cell.getColumnIndex() + 1);
        anchor.setRow1(row.getRowNum());
        anchor.setRow2(row.getRowNum() + 4);

        Comment comment = drawing.createCellComment(anchor);
        RichTextString str = factory.createRichTextString(commentString);
        comment.setString(str);
        return comment;
    }

    @Override
    protected GenericDAO getDAO() {
        return findingDAO;
    }

    @Resource(name = "findingDAO")
    public void setFindingDAO(FindingDAO findingDAO) {
        this.findingDAO = findingDAO;
    }
}
