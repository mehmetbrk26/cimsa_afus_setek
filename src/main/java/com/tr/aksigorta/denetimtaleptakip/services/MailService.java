package com.tr.aksigorta.denetimtaleptakip.services;

import com.tr.aksigorta.denetimtaleptakip.model.Finding;
import com.tr.aksigorta.denetimtaleptakip.model.FindingAction;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: mhazer
 * Date: 8/22/12
 * Time: 5:01 PM
 */
public interface MailService
{
    public void sendNotificationEmails();

    void sendEmailToFindingActionOwnersAndResponsibleHeads(Finding finding, String templateName);

    @Async
    @Transactional(readOnly = true)
    void sendFindingActionUpdateEmail(FindingAction findingAction);

    @Async
    @Transactional(readOnly = true)
    public void sendOpeningEmailToFindingActionOwnersAndResponsibleHeads(Finding finding, String templateName);
}
