package com.tr.aksigorta.denetimtaleptakip.services;

/**
 * User: mhazer
 * Date: 7/29/13
 * Time: 3:59 PM
 */
public interface LoggingServis extends BaseService
{
    public <T> T mergeLog(T detachedEntity);
}
