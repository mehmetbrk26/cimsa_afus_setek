package com.tr.aksigorta.denetimtaleptakip.services;

public interface BaseService
{
	public <T> void reattachUsingLock(T detachedEntity);
	
	public <T> void reattachReadOnly(T detachedEntity);
}
