package com.tr.aksigorta.denetimtaleptakip.services;

import com.tr.aksigorta.denetimtaleptakip.controller.admin.SearchParameters;
import com.tr.aksigorta.denetimtaleptakip.model.*;

import org.primefaces.model.SortOrder;

import java.util.List;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:04 PM
 */
public interface FindingService extends BaseService
{
    public List<AuditType> retrieveAuditTypes();
    
    public List<Company> retrieveCompanies();

    public AuditType retrieveAuditType(String description);
    
    public Company retrieveCompany(String description);

    public List<BusinessProcessType> retrieveBusinessProcesses();

    public Finding mergeFinding(Finding finding);

    public List<Finding> retrieveAllFindings();

    public List<FindingAction> retrieveUserFindingActions(User user);

    public void mergeFindingAction(FindingAction selectedFindingAction);

    public void mergeFindingResponsibleAction(FindingResponsibleAction selectedFindingResponsibleAction);

    public Finding retrieveFinding(Integer findingIdToUpdate);

    public Finding retrieveFinding(String findingNumber);

    public List<BusinessSubProcessType> retrieveBusinessSubProcesses();

    public List<Finding> searchFindings(SearchParameters searchParameters, User user);

    public List<Finding> retrieveNotCompletedFindings();

    public FindingAction retrieveFindingActionWithHistory(Integer findingActionId);

    public List<FindingAction> retrieveHistoryOfFindingAction(Integer findingActionId);

    public List<FindingResponsibleAction> retrieveHistoryOfFindingResponsibleAction(Integer findingResponsibleActionId);

    public AuditType mergeAuditType(AuditType auditType, List<BusinessProcessType> businessProcessTypes, List<User> target);

    public BusinessProcessType mergeBusinessType(BusinessProcessType businessProcessType, List<User> responsibleHeads);

    public List<Finding> retrieveUserResponsibleDepartmentsFindingActions(User user);

    List<AuditType> retrieveAllActiveAuditTypes();
    
    List<Company> retrieveAllActiveCompanies();

    public List<EmailSendingStrategy> retrieveAllEmailStrategies();

    public List<EmailContent> retrieveAllEmailContents();

    public void reassignFinding(Finding finding, List<User> oldActionOwners, List<User> selectedOldActionOwners, User selectedNewActionOwner);

    public BusinessProcessType retrieveBusinessProcessType(String description);

    List<Finding> retrieveAllFindings(int first, int pageSize, String sortField, SortOrder sortOrder);

    Long retrieveAllFindingsCount();

    Long retrieveUserResponsibleDepartmentsFindingActionsCount(User user);

    List<Finding> retrieveUserResponsibleDepartmentsFindingActions(int first, int pageSize, String sortField, SortOrder sortOrder, User user);

    Long retrieveUserFindingActionsCount(User user);

    List<FindingAction> retrieveUserFindingActions(int first, int pageSize, String sortField, SortOrder sortOrder, User user);
    
    public AuditType findByProperty(Class<?> clazz, String propertyName, String value);
    
    public void deleteFinding(Finding finding);

}
