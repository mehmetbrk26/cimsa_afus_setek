package com.tr.aksigorta.denetimtaleptakip.services.impl;

import com.tr.aksigorta.denetimtaleptakip.dao.GenericDAO;

public abstract class BaseServiceImpl
{
    protected abstract GenericDAO getDAO();

    public <T> void reattachUsingLock(T detachedEntity)
    {
        getDAO().reattachUsingLock(detachedEntity);
    }

    public <T> void reattachReadOnly(T detachedEntity)
    {
        getDAO().reattachUsingLock(detachedEntity);
        getDAO().getSession().setReadOnly(detachedEntity, true);
    }
}
