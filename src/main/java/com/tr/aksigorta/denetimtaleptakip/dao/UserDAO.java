package com.tr.aksigorta.denetimtaleptakip.dao;

import com.tr.aksigorta.denetimtaleptakip.model.User;

import java.util.List;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:07 PM
 */
public interface UserDAO extends GenericDAO
{
    public List<User> retrieveAllUsers();

    public List<User> retrieveActiveUsers();
    
	public List<User> findByUsername(String username);
}
