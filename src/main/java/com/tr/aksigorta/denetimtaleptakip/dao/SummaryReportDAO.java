package com.tr.aksigorta.denetimtaleptakip.dao;

import java.math.BigDecimal;
import java.util.*;

import com.tr.aksigorta.denetimtaleptakip.model.*;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:07 PM
 */
public interface SummaryReportDAO extends GenericDAO
{
    public List<Finding> getFindingForSummaryReport(AuditType auditType, User user);
    public List<Finding> getFindingForSummaryReport(AuditType auditType, User user, String priority, String status);
    //public BigDecimal findKpiRateFromTable(BigDecimal higherOverdueKPIRate, BigDecimal lowerOverdueKPIRate);
}
