package com.tr.aksigorta.denetimtaleptakip.dao;

import com.tr.aksigorta.denetimtaleptakip.controller.admin.SearchParameters;
import com.tr.aksigorta.denetimtaleptakip.model.*;

import org.primefaces.model.SortOrder;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:07 PM
 */
public interface FindingDAO extends GenericDAO
{
    public List<Finding> retrieveAllFindings();

    public List<FindingAction> retrieveUserFindingActions(User user);

    public List<BusinessSubProcessType> retrieveBusinessSubProcesses();

    public List<BusinessProcessType> retrieveBusinessProcesses();

    public List<Finding> searchFindings(SearchParameters searchParameters);

    public FindingAction retrieveFindingActionWithHistory(Integer findingActionId);

    public List<FindingAction> retrieveHistoryOfFindingAction(Integer findingActionId);

    public Long findNotCompletedFindingActionCount(Finding finding);

    public List<Company> retrieveAllCompanies();
    
    public List<AuditType> retrieveAllAuditTypes();

    public Company retrieveCompany(String description);
    
    public AuditType retrieveAuditType(String description);

    public List<Finding> retrieveUserResponsibleBusinessTypeFindings(Collection<BusinessProcessType> responsibleBusinessTypes);

    public List<Finding> retrieveUserResponsibleBusinessSubTypeFindings(Collection<BusinessSubProcessType> responsibleBusinessSubTypes);

    List<Company> retrieveAllActiveCompanies();
    
    List<AuditType> retrieveAllActiveAuditTypes();

    public void deleteFindingActionsOfUsers(Finding finding, List<User> selectedOldActionOwners);

    List<FindingResponsibleAction> retrieveHistoryOfFindingResponsibleAction(Integer findingResponsibleActionId);

    public BusinessProcessType retrieveBusinessProcessType(String description);

    public Finding retrieveFinding(String findingNumber);

    List<Finding> retrieveAllFindings(int first, int pageSize, String sortField, SortOrder sortOrder);

    Long retrieveAllFindingsCount();

    List<Finding> retrieveUserResponsibleDepartmentsFindingActions(int first, int pageSize, String sortField, SortOrder sortOrder, User user);

    Long retrieveUserResponsibleDepartmentsFindingActionsCount(User user);

    Long retrieveUserFindingActionsCount(User user);

    List<FindingAction> retrieveUserFindingActions(int first, int pageSize, String sortField, SortOrder sortOrder, User user);
    
    public void deleteFinding(Finding finding);

}
