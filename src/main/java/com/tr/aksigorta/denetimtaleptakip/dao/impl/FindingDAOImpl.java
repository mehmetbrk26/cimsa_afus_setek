package com.tr.aksigorta.denetimtaleptakip.dao.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.tr.aksigorta.denetimtaleptakip.controller.admin.SearchParameters;
import com.tr.aksigorta.denetimtaleptakip.dao.FindingDAO;
import com.tr.aksigorta.denetimtaleptakip.model.*;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.*;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:07 PM
 */
@Repository("findingDAO")
public class FindingDAOImpl extends GenericDAOImpl implements FindingDAO {
    @Override
    public List<Finding> retrieveAllFindings() {
        return getSession()
                .createCriteria(Finding.class)
                .addOrder(Order.asc("completionDate"))
                .list();
    }

    @Override
    public List<FindingAction> retrieveUserFindingActions(User user) {
        return getSession()
                .createCriteria(FindingAction.class)
                .add(Restrictions.eq("actionOwner", user))
                .addOrder(Order.asc("completionPercentage"))
                .list();
    }

    @Override
    public List<BusinessSubProcessType> retrieveBusinessSubProcesses() {
        return getSession()
                .createCriteria(BusinessSubProcessType.class)
                .list();
    }

    @Override
    public List<BusinessProcessType> retrieveBusinessProcesses() {
        return getSession()
                .createCriteria(BusinessProcessType.class)
                .list();
    }

    @Override
    public List<Finding> searchFindings(SearchParameters searchParameters) {
        DetachedCriteria detachedCriteria = searchParameters.createSearhQueryFromParameters();
        Criteria criteria = detachedCriteria.getExecutableCriteria(getSession());
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    @Override
    public FindingAction retrieveFindingActionWithHistory(Integer findingActionId) {
        FindingAction findingAction = findById(FindingAction.class, findingActionId);
        Preconditions.checkNotNull(findingAction, "Can not find action with given id %s", findingActionId);
        findingAction.setHistoryOfFindingAction(retrieveHistoryOfFindingAction(findingActionId));
        return findingAction;
    }

    @Override
    public List<FindingAction> retrieveHistoryOfFindingAction(Integer findingActionId) {
        AuditQuery auditQuery =
                AuditReaderFactory.get(getSession())
                        .createQuery()
                        .forRevisionsOfEntity(FindingAction.class, true, false)
                        .add(AuditEntity.id().eq(findingActionId));
        return auditQuery.getResultList();
    }

    @Override
    public Long findNotCompletedFindingActionCount(Finding finding) {
        return (Long) getSession()
                .createCriteria(FindingAction.class)
                .add(Restrictions.eq("finding", finding))
                .add(Restrictions.lt("completionPercentage", 100))
                .setProjection(Projections.rowCount())
                .uniqueResult();
    }

    @Override
    public List<AuditType> retrieveAllAuditTypes() {
        return getSession()
                .createCriteria(AuditType.class)
                .addOrder(Order.asc("description"))
                .list();
    }
    
    @Override
    public List<Company> retrieveAllCompanies() {
        return getSession()
                .createCriteria(Company.class)
                .addOrder(Order.asc("description"))
                .list();
    }

    @Override
    public AuditType retrieveAuditType(String description) {
        return findByProperty(AuditType.class, "description", description);
    }
    
    @Override
    public Company retrieveCompany(String description) {
        return findByProperty(Company.class, "description", description);
    }

    @Override
    public List<AuditType> retrieveAllActiveAuditTypes() {
        return getSession()
                .createCriteria(AuditType.class)
                .add(Restrictions.eq("active", true))
                .addOrder(Order.asc("description"))
                .list();
    }
    
    @Override
    public List<Company> retrieveAllActiveCompanies() {
        return getSession()
                .createCriteria(Company.class)
                .add(Restrictions.eq("active", true))
                .addOrder(Order.asc("description"))
                .list();
    }

    @Override
    public List<Finding> retrieveUserResponsibleBusinessTypeFindings(Collection<BusinessProcessType> responsibleBusinessTypes) {
        return getSession()
                .createCriteria(Finding.class)
                .add(Restrictions.in("businessProcessType", responsibleBusinessTypes))
                .list();
    }

    @Override
    public List<Finding> retrieveUserResponsibleBusinessSubTypeFindings(Collection<BusinessSubProcessType> responsibleBusinessSubTypes) {
        return getSession()
                .createCriteria(Finding.class)
                .add(Restrictions.in("businessSubProcessType", responsibleBusinessSubTypes))
                .list();
    }

    @Override
    public void deleteFindingActionsOfUsers(Finding finding, List<User> selectedOldActionOwners) {
        if (CollectionUtils.isNotEmpty(selectedOldActionOwners)) {
            getSession()
                    .getNamedQuery("finding.deleteFindingActions")
                    .setParameter("finding", finding)
                    .setParameterList("oldUsers", selectedOldActionOwners)
                    .executeUpdate();
        }
    }

    @Override
    public List<FindingResponsibleAction> retrieveHistoryOfFindingResponsibleAction(Integer findingResponsibleActionId) {
        AuditQuery auditQuery =
                AuditReaderFactory.get(getSession())
                        .createQuery()
                        .forRevisionsOfEntity(FindingResponsibleAction.class, true, false)
                        .add(AuditEntity.id().eq(findingResponsibleActionId));
        return auditQuery.getResultList();
    }

    @Override
    public BusinessProcessType retrieveBusinessProcessType(String description) {
        return findByProperty(BusinessProcessType.class, "description", description);
    }

    @Override
    public Finding retrieveFinding(String findingNumber) {
        return findByProperty(Finding.class, "findingNumber", findingNumber);
    }

    @Override
    public List<Finding> retrieveAllFindings(int first, int pageSize, String sortField, SortOrder sortOrder) {
        Criteria criteria = getSession().createCriteria(Finding.class);
        criteria.createAlias("businessProcessType","businessProcessType");
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }
        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (sortField == null) {
            criteria.addOrder(Order.asc("completionDate"));
        } else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc(sortField));
        }
        return criteria.list();
    }

    @Override
    public Long retrieveAllFindingsCount() {
        final Criteria criteria = getSession().createCriteria(Finding.class);
        criteria.setProjection(Projections.rowCount());
        final Long result = (Long) criteria.uniqueResult();
        return result;
    }

    @Override
    public List<Finding> retrieveUserResponsibleDepartmentsFindingActions(int first, int pageSize, String sortField, SortOrder sortOrder, User user) {
        Criteria criteria = getSession().createCriteria(Finding.class);
        criteria.createAlias("businessProcessType","businessProcessType");
        if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessTypes()) && CollectionUtils.isNotEmpty(user.getResponsibleBusinessSubTypes())) {
            Criterion businessProcessTypeCriterion = Restrictions.in("businessProcessType", user.getResponsibleBusinessTypes());
            Criterion businessSubProcessTypeCriterion = Restrictions.in("businessSubProcessType", user.getResponsibleBusinessSubTypes());
            criteria.add(Restrictions.or(businessProcessTypeCriterion, businessSubProcessTypeCriterion));
        } else if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessTypes())) {
            criteria.add(Restrictions.in("businessProcessType", user.getResponsibleBusinessTypes()));
        } else if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessSubTypes())) {
            criteria.add(Restrictions.in("businessSubProcessType",  user.getResponsibleBusinessSubTypes()));
        } else {
            return Lists.newLinkedList();
        }

        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }
        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (sortField == null) {
            criteria.addOrder(Order.asc("completionDate"));
        } else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc(sortField));
        }
        return criteria.list();
    }

    @Override
    public Long retrieveUserResponsibleDepartmentsFindingActionsCount(User user) {
        Criteria criteria = getSession().createCriteria(Finding.class);

        if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessTypes()) && CollectionUtils.isNotEmpty(user.getResponsibleBusinessSubTypes())) {
            Criterion businessProcessTypeCriterion = Restrictions.in("businessProcessType", user.getResponsibleBusinessTypes());
            Criterion businessSubProcessTypeCriterion = Restrictions.in("businessSubProcessType", user.getResponsibleBusinessSubTypes());
            criteria.add(Restrictions.or(businessProcessTypeCriterion, businessSubProcessTypeCriterion));
        } else if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessTypes())) {
            criteria.add(Restrictions.in("businessProcessType", user.getResponsibleBusinessTypes()));
        } else if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessSubTypes())) {
            criteria.add(Restrictions.in("businessSubProcessType",  user.getResponsibleBusinessSubTypes()));
        } else {
            return (long) 0;
        }

        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.uniqueResult();
    }

    @Override
    public Long retrieveUserFindingActionsCount(User user) {
        Criteria criteria = getSession().createCriteria(FindingAction.class);
        criteria.add(Restrictions.eq("actionOwner", user));
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.uniqueResult();
    }

    @Override
    public List<FindingAction> retrieveUserFindingActions(int first, int pageSize, String sortField, SortOrder sortOrder, User user) {
        final Criteria criteria = getSession().createCriteria(FindingAction.class);
        criteria.add(Restrictions.eq("actionOwner", user));
        criteria.createAlias("finding","finding");

        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }
        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (sortField == null) {
            criteria.addOrder(Order.asc("completionPercentage"));
        }
        else if( sortField.equals("custom"))
        {
        	criteria.addOrder(Order.asc("finding.completed"));
        	criteria.addOrder(Order.asc("finding.confirmed"));
        	criteria.addOrder(Order.asc("finding.completionDate"));
        	criteria.addOrder(Order.asc("completionPercentage"));
        }
        else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc(sortField));
        }

        return  criteria.list();
    }

	@Override
	public void deleteFinding(Finding finding) {
		getSession().beginTransaction();
		getSession().delete(finding);
		getSession().flush();
		getSession().getTransaction().commit();
	}


}
