package com.tr.aksigorta.denetimtaleptakip.model;

/**
 * 
 * @author mhazer
 * 
 * */
public class EmailReasonUserType extends EnumUserType<EmailReason> {

	public EmailReasonUserType() {
		super(EmailReason.class);
	}
}
