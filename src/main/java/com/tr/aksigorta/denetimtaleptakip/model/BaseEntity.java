package com.tr.aksigorta.denetimtaleptakip.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

@Data
@MappedSuperclass
@EqualsAndHashCode(exclude = {"id", "version"}, callSuper = false)
public class BaseEntity implements Serializable
{
    @Id
    @Column(name="ID")
    @GeneratedValue(strategy= GenerationType.AUTO)
    protected Integer id;
    @Version
    protected Integer version;
}