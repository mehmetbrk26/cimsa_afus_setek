package com.tr.aksigorta.denetimtaleptakip.model;

import com.google.common.collect.Sets;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 4:58 PM
 */
@Entity
@Table(name = "DT_ID_AUDIT_TYPE_TABLE")
@Data
@ToString(includeFieldNames = true, callSuper = true, exclude = {"businessProcessTypes", "auditTypeUpdateFollowers"})
@EqualsAndHashCode(callSuper = true, exclude = {"businessProcessTypes", "sendOpeningEmail", "emailSendingStrategy", "emailStrategyContentOne",
        "emailStrategyContentTwo","emailStrategyContentThree","updateEmailContent", "openingEmailContent", "auditTypeUpdateFollowers"})
public class AuditType extends BaseDefinitionEntity implements Serializable
{
    public Set<BusinessProcessType> getBusinessProcessTypes() {
		return businessProcessTypes;
	}

	public void setBusinessProcessTypes(Set<BusinessProcessType> businessProcessTypes) {
		this.businessProcessTypes = businessProcessTypes;
	}

	@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "DT_ID_AUDIT_BUSINESS_TABLE",
            joinColumns =
            @JoinColumn(name = "AUDIT_ID", referencedColumnName = "ID"),
            inverseJoinColumns =
            @JoinColumn(name = "BUSINESS_TYPE_ID", referencedColumnName = "ID")
    )
    private Set<BusinessProcessType> businessProcessTypes = Sets.newHashSet();

    @Column(name = "SEND_OPENING_MAIL", nullable = false, columnDefinition = "BIT", length = 1)
    private boolean sendOpeningEmail;

    @Column(name = "EMAIL_SUBJECT", nullable = false)
    private String emailSubject;

    @ManyToOne
    @JoinColumn(name = "EMAIL_SENDING_STRATEGY")
    private EmailSendingStrategy emailSendingStrategy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMAIL_STRATEGY_CONTENT_ONE_ID")
    @NotFound(action = NotFoundAction.IGNORE)
    private EmailContent emailStrategyContentOne;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMAIL_STRATEGY_CONTENT_TWO_ID")
    @NotFound(action = NotFoundAction.IGNORE)
    private EmailContent emailStrategyContentTwo;

    @ManyToOne
    @JoinColumn(name = "UPDATE_EMAIL_CONTENT_ID")
    @NotFound(action = NotFoundAction.IGNORE)
    private EmailContent updateEmailContent;

    @ManyToOne
    @JoinColumn(name = "OPENING_EMAIL_CONTENT_ID")
    @NotFound(action = NotFoundAction.IGNORE)
    private EmailContent openingEmailContent;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMAIL_STRATEGY_CONTENT_THREE_ID")
    @NotFound(action = NotFoundAction.IGNORE)
    private EmailContent emailStrategyContentThree;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "DT_ID_AUDIT_TYP_FOLLOWER_TABLE",
            joinColumns =
            @JoinColumn(name = "AUDIT_TYPE_ID", referencedColumnName = "ID"),
            inverseJoinColumns =
            @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    )
    @NotFound(action = NotFoundAction.IGNORE)
    private Set<User> auditTypeUpdateFollowers = Sets.newHashSet();

    @Transient
    public List<User> getResponsibleHeadsAsList()
    {
        return Collections.emptyList();
    }

    @Column(name = "COMPANY_REQUIRED", nullable = false, columnDefinition = "BIT", length = 1)
    private boolean companyRequired;

    @Transient
    public boolean getCompanyRequired()
    {
        return companyRequired;
    }
}
