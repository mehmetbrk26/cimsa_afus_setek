package com.tr.aksigorta.denetimtaleptakip.model;

/**
 * 
 * @author mhazer
 * 
 * */
public class PriorityUserType extends EnumUserType<Priority> {

	public PriorityUserType() {
		super(Priority.class);
	}
}
