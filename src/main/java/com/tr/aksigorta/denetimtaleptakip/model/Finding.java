package com.tr.aksigorta.denetimtaleptakip.model;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.tr.aksigorta.denetimtaleptakip.controller.util.CalendarUtil;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Type;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.persistence.Transient;

import java.beans.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * User: mhazer
 * Date: 8/20/12
 * Time: 11:49 AM
 */
@Table(name = "DT_RT_FINDINGS_TABLE")
@Entity
@Data
@ToString(includeFieldNames = true, callSuper = true, exclude = {"findingActions", "findingResponsibleActions", "auditUser"})
@EqualsAndHashCode(callSuper = true, exclude = {"findingActions", "findingResponsibleActions", "auditUser"})
public class Finding extends BaseEntity implements Serializable {

    @ManyToOne
    @JoinColumn(name = "AUDIT_TYPE")
    private AuditType auditType;

    @ManyToOne
    @JoinColumn(name = "COMPANY_ID")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "BUSINESS_PROC")
    private BusinessProcessType businessProcessType;

    @ManyToOne
    @JoinColumn(name = "BUSINESS_SUB_PROC")
    private BusinessSubProcessType businessSubProcessType;

    @Column(name = "FINDING_NUMBER")
    private String findingNumber;

    @Lob
    private String description;

    @Type(type = "com.tr.aksigorta.denetimtaleptakip.model.PriorityUserType")
    private Priority priority;

    @Lob
    private String detail;

    private String risk;

    @Lob
    private String recommendation;

    @Column(name = "MANAGEMENT_ACTION_PLAN")
    @Lob
    private String managementActionPlan;

    @Column(name = "COMPLETION_DATE")
    private Date completionDate;

    @Column(name = "SEND_EMAIL", columnDefinition = "BIT", length = 1)
    private Boolean sendEmail = Boolean.TRUE;

    @Column(name = "REASON_TO_NOT_SEND_EMAIL")
    @Type(type = "com.tr.aksigorta.denetimtaleptakip.model.EmailReasonUserType")
    private EmailReason reasonToNotSendEmail;

    @Column(name = "COMPLETED", columnDefinition = "BIT", length = 1)
    private Boolean completed = Boolean.FALSE;

    @Column(name = "CONFIRMED", columnDefinition = "BIT", length = 1)
    private Boolean confirmed = Boolean.FALSE;

    @Column(name = "REVISED", columnDefinition = "BIT", length = 1)
    private Boolean revised = Boolean.FALSE;

    @Column(name = "AUDIT_DATE")
    private Date auditDate;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "FINDING_YEAR", length=4)
    private Date findingYear;

    @Column(name = "REVISED_COMPLETION_DATE")
    private Date revisedCompletionDate;
    
    @Column(name = "REPORT_NAME")
    private String reportName;

    @ManyToOne
    @JoinColumn(name = "AUDIT_USER")
    private User auditUser;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "finding", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("changeDate desc")
    private Set<FindingAction> findingActions = Sets.newHashSet();

   
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "finding", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("changeDate desc")
    private Set<FindingResponsibleAction> findingResponsibleActions = Sets.newHashSet();

    public Set<FindingResponsibleAction> getFindingResponsibleActions() {
		return findingResponsibleActions;
	}

	public void setFindingResponsibleActions(Set<FindingResponsibleAction> findingResponsibleActions) {
		this.findingResponsibleActions = findingResponsibleActions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "finding", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("changeDate")
    private Set<FindingFile> findingFiles = Sets.newHashSet();

    @Transient
    public List<FindingAction> findingActionsAsList() {
        return Lists.newLinkedList(getFindingActions());
    }

    @Transient
    public List<FindingResponsibleAction> findingResponsibleActionsAsList() {
        return Lists.newLinkedList(getFindingResponsibleActions());
    }

    @Transient
    public List<FindingFile> findingFilesAsList() {
        return Lists.newLinkedList(getFindingFiles());
    }

    @Transient
    public void adjustFindingActionsForUsers(List<User> actionOwners, List<User> readOnlyActionOwners) {
        addFindingActionsToOwners(actionOwners, false);
        addFindingActionsToOwners(readOnlyActionOwners, true);
        adjustFindingActionsCompletionDates(actionOwners, readOnlyActionOwners);
    }

    @Transient
    public void adjustFindingResponsibleActionsForUser(User responsibleActionOwner) {
        addFindingResponsibleActionsToOwners(responsibleActionOwner);
    }

    private void addFindingActionsToOwners(List<User> actionOwners, boolean readOnly) {
        for (User _actionOwner : actionOwners) {
            if (!isFindingActionExistsForUser(_actionOwner)) {
                FindingAction findingAction = new FindingAction();
                findingAction.setFinding(this);
                findingAction.setActionOwner(_actionOwner);
                findingAction.setReadOnly(readOnly);
                getFindingActions().add(findingAction);
            } else {
                findingActionOfUser(_actionOwner).setReadOnly(readOnly);
            }
        }
    }

    private void addFindingResponsibleActionsToOwners(User responsibleActionOwner) {
        if (!isFindingResponsibleActionExistsForUser(responsibleActionOwner)) {
            FindingResponsibleAction findingResponsibleAction = new FindingResponsibleAction();
            findingResponsibleAction.setFinding(this);
            findingResponsibleAction.setActionOwner(responsibleActionOwner);
            getFindingResponsibleActions().add(findingResponsibleAction);
        }
    }

    @Transient
    public boolean isFindingActionExistsForUser(User _user) {
        for (FindingAction findingAction : getFindingActions()) {
            if (findingAction.getActionOwner().getId().equals(_user.getId())) {
                return true;
            }
        }
        return false;
    }

    @Transient
    public boolean isFindingResponsibleActionExistsForUser(User _user) {
        for (FindingResponsibleAction findingResponsibleAction : getFindingResponsibleActions()) {
            if (findingResponsibleAction.getActionOwner().getId().equals(_user.getId())) {
                return true;
            }
        }
        return false;
    }

    @Transient
    public FindingAction findingActionOfUser(User _user) {
        for (FindingAction findingAction : getFindingActions()) {
            if (findingAction.getActionOwner().getId().equals(_user.getId())) {
                return findingAction;
            }
        }
        return null;
    }

    @Transient
    public FindingResponsibleAction findingResponsibleActionOfUser(User _user) {
        for (FindingResponsibleAction findingResponsibleAction : getFindingResponsibleActions()) {
            if (findingResponsibleAction.getActionOwner().getId().equals(_user.getId())) {
                return findingResponsibleAction;
            }
        }
        return null;
    }

    @Transient
    private void adjustFindingActionsCompletionDates(List<User> actionOwners, List<User> readOnlyActionOwners) {
        for (Iterator<FindingAction> iterator = getFindingActions().iterator(); iterator.hasNext(); ) {
            FindingAction findingAction = iterator.next();
            if (!sendEmail) {
                findingAction.setCompletionPercentage(100);
                setCompleted(Boolean.TRUE);
                findingAction.setActionTaken(StringUtils.EMPTY);
            } else if (findingAction.isReadOnly()) {
                findingAction.setCompletionPercentage(100);
                setReasonToNotSendEmail(null);
                findingAction.setReadOnly(Boolean.TRUE);
                findingAction.setActionTaken("Read Only User");
            } else if (findingAction.getId() == null) {
                setCompleted(Boolean.FALSE);
                setReasonToNotSendEmail(null);
                findingAction.setCompletionPercentage(0);
                findingAction.setActionTaken(StringUtils.EMPTY);
            }
            if (!actionOwners.contains(findingAction.getActionOwner()) && !readOnlyActionOwners.contains(findingAction.getActionOwner())) {
                iterator.remove();
            }
        }
    }

    @Transient
    public Set<FindingAction> retainPersistedFindingActions() {
        return Sets.newHashSet(
                Iterables.filter(getFindingActions(), new Predicate<FindingAction>() {
                    @Override
                    public boolean apply(@Nullable FindingAction findingAction) {
                        return findingAction.getId() != null;
                    }
                }));
    }

    @Transient
    public Set<FindingResponsibleAction> retainPersistedFindingResponsibleActions() {
        return Sets.newHashSet(
                Iterables.filter(getFindingResponsibleActions(), new Predicate<FindingResponsibleAction>() {
                    @Override
                    public boolean apply(@Nullable FindingResponsibleAction findingResponsibleAction) {
                        return findingResponsibleAction.getId() != null;
                    }
                }));
    }

    @Transient
    public Status getStatusOfFinding() {
        if (confirmed) {
            if (completed) {
                return Status.CONFIRMED_COMPLETED;
            } else {
                int completionPercentage = getCompletionPercentageFromResponsibleActions();
                return getStatus(completionPercentage);
            }
        } else if (completed) {
            return Status.UNCONFIRMED_COMPLETED;
        } else {
            int completionPercentage = getCompletionPercentageFromActions();
            return getStatus(completionPercentage);
        }
    }

    @Transient
    public int getCompletionPercentage() {
        if (confirmed) {
            int completionPercentage = getCompletionPercentageFromResponsibleActions();
            return completionPercentage;
        } else {
            int completionPercentage = getCompletionPercentageFromActions();
            return completionPercentage;
        }
    }

    @Transient
    private Status getStatus(int completionPercentage) {
        LocalDate dayToDefineDelayStatus = defineDayToFindDelayedStatus();
        LocalDate todayDate = new LocalDate();
        if (todayDate.isAfter(dayToDefineDelayStatus)) {
            return Status.DELAYED;
        } else if (completionPercentage == 0) {
            return Status.NOT_STARTED;
        } else {
            return Status.IN_PROGRESS;
        }
    }

    @Transient
    public int getCompletionPercentageFromActions() {
        int completionPercentage = 0;
        DateTime changeDate = null;
        for (FindingAction action : getFindingActions()) {
            //son girilen alinacak, hem readonly olmayacak hem de changedate olacak
            if(!action.isReadOnly() && action.getChangeDate() != null) {
                changeDate = new DateTime(action.getChangeDate());
                completionPercentage = action.getCompletionPercentage();
                break;
            }
        }
        for (FindingResponsibleAction action : getFindingResponsibleActions()) {
            //son girilen alinacak, hem de changedate olacak
            if (changeDate == null ||
                    (action.getChangeDate() != null &&
                     new DateTime(action.getChangeDate()).isAfter(changeDate))) {
                completionPercentage = action.getCompletionPercentage();
                break;
            }
        }
        return completionPercentage;
    }

    @Transient
    public String getActionTakenFromActions() {
        String actionTaken = "";
        for (FindingAction action : getFindingActions()) {
            //son girilen alinacak, hem readonly olmayacak hem de changedate olacak
            if(!action.isReadOnly() && action.getChangeDate()!= null){
                actionTaken = action.getActionTaken();
                break;
            }
        }
        return actionTaken;
    }

    @Transient
    public int getCompletionPercentageFromResponsibleActions() {
        int completionPercentage = 0;
        for (FindingResponsibleAction responsibleAction : getFindingResponsibleActions()) {
            //son girilen alinacak
            completionPercentage = responsibleAction.getCompletionPercentage();
            break;
        }
        return completionPercentage;
    }

    @Transient
    public String getActionTakenFromResponsibleActions() {
        String actionTaken = "";
        for (FindingResponsibleAction responsibleAction : getFindingResponsibleActions()) {
            //son girilen alinacak, hem de changedate olacak
            if(responsibleAction.getChangeDate()!= null){
                actionTaken = responsibleAction.getActionTaken();
                break;
            }
        }
        return actionTaken;
    }

    @Transient
    public LocalDate defineDayToFindDelayedStatus() {
        LocalDate dayToDefineDelayStatus;
        if (revised && getRevisedCompletionDate() != null) {
            dayToDefineDelayStatus = new LocalDate(getRevisedCompletionDate());
        }else {
            dayToDefineDelayStatus = new LocalDate(getCompletionDate());
        }
        return dayToDefineDelayStatus;
    }

    @Transient
    public List<User> usersOfFindingActions() {
        List<User> users = Lists.newArrayList();
        for (FindingAction findingAction : getFindingActions()) {
            users.add(findingAction.getActionOwner());
        }
        return users;
    }

    @Transient
    public List<User> usersOfFindingResponsibleActions() {
        List<User> users = Lists.newArrayList();
        for (FindingResponsibleAction findingResponsibleAction : getFindingResponsibleActions()) {
            users.add(findingResponsibleAction.getActionOwner());
        }
        return users;
    }
    
    @Transient
    public int getCountOfDelayedDay()
    {
        if (this.getCompletionDate() != null & Status.DELAYED.equals(this.getStatusOfFinding()))
        {
           return  CalendarUtil.sistemTarihiyleArasindakiGunFarki(this.completionDate) >  0 ?
                   CalendarUtil.sistemTarihiyleArasindakiGunFarki(this.completionDate) : 0;
        }
        return 0;
    }
}
