package com.tr.aksigorta.denetimtaleptakip.dto;

import com.tr.aksigorta.denetimtaleptakip.model.Priority;
import lombok.Data;

import java.util.Date;

/**
 * Created by gkisakol on 4/4/2016.
 */
@Data
public class SummaryReportDto
{
    private String findingNumber;
    private String description;
    private Priority priority;private Date completionDate;

}
