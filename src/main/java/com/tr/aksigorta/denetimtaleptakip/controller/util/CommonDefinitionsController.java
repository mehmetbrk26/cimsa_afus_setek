package com.tr.aksigorta.denetimtaleptakip.controller.util;

import com.tr.aksigorta.denetimtaleptakip.model.AuditType;
import com.tr.aksigorta.denetimtaleptakip.model.BusinessProcessType;
import com.tr.aksigorta.denetimtaleptakip.model.BusinessSubProcessType;
import com.tr.aksigorta.denetimtaleptakip.model.Company;
import com.tr.aksigorta.denetimtaleptakip.model.EmailContent;
import com.tr.aksigorta.denetimtaleptakip.model.EmailSendingStrategy;
import com.tr.aksigorta.denetimtaleptakip.model.Priority;
import com.tr.aksigorta.denetimtaleptakip.model.Role;
import com.tr.aksigorta.denetimtaleptakip.model.User;
import com.tr.aksigorta.denetimtaleptakip.services.FindingService;
import com.tr.aksigorta.denetimtaleptakip.services.UserService;
import lombok.Getter;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * User: mhazer
 * Date: 8/21/12
 * Time: 11:35 PM
 */
@ManagedBean(name = "commonDefinitionsController")
@SessionScoped
public class CommonDefinitionsController implements Serializable
{
    @ManagedProperty("#{findingService}")
    private FindingService findingService;

    @ManagedProperty("#{userService}")
    private UserService userService;

    @Getter
    private List<AuditType> auditTypes;
    
    @Getter
    private List<Company> companies;

    @Getter
    private List<AuditType> activeAuditTypes;
    
    @Getter
    private List<Company> activeCompanies;

    @Getter
    private List<BusinessProcessType> businessProcessTypes;

    @Getter
    private List<BusinessSubProcessType> businessSubProcessTypes;

    @Getter
    private List<Priority> priorities;

    @Getter
    private List<User> activeUsers;

    @Getter
    private List<User> allUsers;

    @Getter
    private List<Role> allRoles;

    @Getter
    private List<EmailSendingStrategy> emailSendingStrategies;

    @Getter
    private List<EmailContent> emailContents;

    @PostConstruct
    public void init()
    {
        this.priorities = Arrays.asList(Priority.values());
        refreshAuditTypes();
        refreshCompanies();
        refreshBusinessTypes();
        refreshUsers();
        refreshRoles();
        refreshEmailSendingStrategies();
        refreshEmailContents();
    }

    public void refreshUsers()
    {
        this.activeUsers = this.userService.retrieveActiveUsers();
        this.allUsers = this.userService.retrieveAllUsers();
    }

    public void refreshAuditTypes()
    {
        this.auditTypes = this.findingService.retrieveAuditTypes();
        this.activeAuditTypes = this.findingService.retrieveAllActiveAuditTypes();
    }
    
    public void refreshCompanies()
    {
        this.companies = this.findingService.retrieveCompanies();
        this.activeCompanies = this.findingService.retrieveAllActiveCompanies();
    }

    public void refreshBusinessTypes()
    {
        this.businessProcessTypes = this.findingService.retrieveBusinessProcesses();
        this.businessSubProcessTypes = this.findingService.retrieveBusinessSubProcesses();
    }

    private void refreshRoles()
    {
        this.allRoles = this.userService.retrieveAllRoles();
    }

    private void refreshEmailSendingStrategies()
    {
        this.emailSendingStrategies = this.findingService.retrieveAllEmailStrategies();
    }

    private void refreshEmailContents()
    {
        this.emailContents = this.findingService.retrieveAllEmailContents();
    }

    public void setFindingService(FindingService findingService)
    {
        this.findingService = findingService;
    }

    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }
}
