package com.tr.aksigorta.denetimtaleptakip.controller.admin;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import lombok.Getter;
import lombok.Setter;

import org.apache.poi.ss.usermodel.Workbook;
import org.javatuples.Quintet;
import org.javatuples.Triplet;
import org.primefaces.component.chart.bar.BarChart;
import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.tr.aksigorta.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.aksigorta.denetimtaleptakip.model.*;
import com.tr.aksigorta.denetimtaleptakip.services.FindingService;
import com.tr.aksigorta.denetimtaleptakip.services.SummaryReportExcelService;
import com.tr.aksigorta.denetimtaleptakip.services.SummaryReportService;


@SuppressWarnings("serial")
@ManagedBean(name = "summaryReportController")
@ViewScoped
public class SummaryReportController implements Serializable
{
    private final static Logger LOGGER = LoggerFactory.getLogger(SummaryReportController.class);

    public static final String UNKNOWN_KPI_RATE = "N/A";
    public static final BigDecimal HIGHER_OVERDUEKPI_MAX_RATE = new BigDecimal("0.15");
    public static final BigDecimal LOWER_OVERDUEKPI_MAX_RATE = new BigDecimal("0.25");

    @Getter @Setter
    private AuditType auditType;

    @Getter
    private CartesianChartModel businessSubProcessChatModel;

    @ManagedProperty("#{summaryReportService}")
    @Setter
    private SummaryReportService summaryReportService;

    @ManagedProperty("#{summaryReportExcelService}")
    @Setter
    private SummaryReportExcelService summaryReportExcelService;

    @ManagedProperty("#{findingService}")
    @Setter
    private FindingService findingService;

    @ManagedProperty("#{visit.user}")
    @Setter
    private User user;

    @Getter @Setter
    private String kpiRate;

    @Getter @Setter
    private BigDecimal higherOverdueKPIRate;

    @Getter @Setter
    private BigDecimal lowerOverdueKPIRate;

    @Getter @Setter
    List<Finding> notStartedFinding = Lists.newArrayList(), 
    		inProgressFinding = Lists.newArrayList(),
            overDueFinding = Lists.newArrayList(),
            confirmedCompletedFinding = Lists.newArrayList(),
            unconfirmedCompletedFinding = Lists.newArrayList(),
            selectedFindings = Lists.newArrayList();

    @PostConstruct
    public void init()
    {
        this.auditType = this.findingService.findByProperty(AuditType.class,
                "description", AfusEnum.AuditType.INTERNAL_AUDIT.getDescription());
        this.createDashboards();
        this.calculateKpiRate();
    }

    private void calculateKpiRate()
    {
        //Verh High+High Findings Rate = (Very High+High  Overdue Findings) / All Open (in progress  + overdue) Findings
        //Moderate+Low Findings Rate = (Moderate+Low Overdue Findings) / All Open (in progress  + overdue) Findings

        /**BigDecimal numberOfAllOpenFindings = new BigDecimal(this.notStartedFinding.size() + this.inProgressFinding.size() + this.overDueFinding.size());
        if (numberOfAllOpenFindings.signum() > 0)
        {
            BigDecimal veryHighOverdue = filterFindingForStatus(this.overDueFinding, Priority.VERY_HIGH);
            BigDecimal highOverdue = filterFindingForStatus(this.overDueFinding, Priority.HIGH);
            BigDecimal moderateOverdue = filterFindingForStatus(this.overDueFinding, Priority.MODERATE);
            BigDecimal lowOverdue = filterFindingForStatus(this.overDueFinding, Priority.LOW);

            this.higherOverdueKPIRate = veryHighOverdue.add(highOverdue).divide(numberOfAllOpenFindings, 4, BigDecimal.ROUND_HALF_UP);
            this.lowerOverdueKPIRate = moderateOverdue.add(lowOverdue).divide(numberOfAllOpenFindings, 4, BigDecimal.ROUND_HALF_UP);
            if (higherOverdueKPIRate.compareTo(HIGHER_OVERDUEKPI_MAX_RATE) > 0 || lowerOverdueKPIRate.compareTo(LOWER_OVERDUEKPI_MAX_RATE) > 0) {
                this.kpiRate = "0";
            }
            else
            {
                this.kpiRate = this.summaryReportService.findKpiRateFromTable(higherOverdueKPIRate, lowerOverdueKPIRate);
            }
            return;
        }**/
       this.kpiRate = UNKNOWN_KPI_RATE;
    }

    public void changeAuditType()
    {
        try
        {
            LOGGER.debug("changing audit type");
            Preconditions.checkArgument(this.auditType != null, "You have to choose audit type first");
            this.createDashboards();
            this.calculateKpiRate();
            LOGGER.debug("changed audit type");
        }
        catch (Exception e)
        {
            this.auditType = null;
            FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
        }
    }

    public void createDashboards()
    {
        try
        {
            this.initializeProperites();
            this.fillStatusOfLists();
            this.createVeryHighChartSeries();
            this.createHighChartSeries();
            this.createModerateChartSeries();
            this.createLowChartSeries();
        }
        catch (Exception e)
        {
            LOGGER.error("createDashboards failed...", e);
            FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
        }
    }

    public void summaryReportListener(ItemSelectEvent event)
    {
        try
        {
            Integer seriesIndex = event.getSeriesIndex();
            Integer itemIndex = event.getItemIndex();
            CartesianChartModel cartesianChartModel = (CartesianChartModel) (((BarChart) event.getSource()).getValue());
            List<ChartSeries> chartSeries = cartesianChartModel.getSeries();
            String priority = chartSeries.get(seriesIndex).getLabel();
            Map<Object, Number> chartData = chartSeries.get(seriesIndex).getData();
            String status = (String)Iterables.get(chartData.keySet(), itemIndex);
            this.findSelectedFindings(priority, status);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            LOGGER.error("summaryReportListener failed...", e);
        }
    }

    private void initializeProperites()
    {
    	this.notStartedFinding.clear();
        this.inProgressFinding.clear();
        this.overDueFinding.clear();
        this.confirmedCompletedFinding.clear();
        this.unconfirmedCompletedFinding.clear();
        this.businessSubProcessChatModel = new CartesianChartModel();
    }

    private void fillStatusOfLists()
    {
    	Quintet<List<Finding>, List<Finding>, List<Finding>, List<Finding>, List<Finding>> summaryReportQuintetResults
                =  this.summaryReportService.getFindingForSummaryReport(this.auditType, user);
    	this.setNotStartedFinding(summaryReportQuintetResults.getValue0());
        this.setInProgressFinding(summaryReportQuintetResults.getValue1());
        this.setOverDueFinding(summaryReportQuintetResults.getValue2());
        this.setConfirmedCompletedFinding(summaryReportQuintetResults.getValue3());
        this.setUnconfirmedCompletedFinding(summaryReportQuintetResults.getValue4());
    }

    private void createVeryHighChartSeries()
    {
        createSeries(Priority.VERY_HIGH.getDescription(), getNumberOfRecords(Priority.VERY_HIGH));
    }

    private void createHighChartSeries()
    {
        createSeries(Priority.HIGH.getDescription(), getNumberOfRecords(Priority.HIGH));
    }

    private void createModerateChartSeries()
    {
        createSeries( Priority.MODERATE.getDescription(), getNumberOfRecords(Priority.MODERATE));
    }

    private void createLowChartSeries()
    {
        createSeries(Priority.LOW.getDescription(), getNumberOfRecords(Priority.LOW));
    }

    private Quintet<BigDecimal, BigDecimal, BigDecimal, BigDecimal, BigDecimal> getNumberOfRecords(Priority priority)
    {
        return  Quintet.with(filterFindingForStatus(this.notStartedFinding, priority),
        			filterFindingForStatus(this.inProgressFinding, priority),
                    filterFindingForStatus(this.overDueFinding, priority),
                    filterFindingForStatus(this.confirmedCompletedFinding, priority),
                    filterFindingForStatus(this.unconfirmedCompletedFinding, priority));
    }

    private void createSeries(String priority, Quintet<BigDecimal, BigDecimal, BigDecimal, BigDecimal, BigDecimal> numberOfRecords)
    {
        ChartSeries chartSeries = new ChartSeries();
        chartSeries.setLabel(priority);
        chartSeries.set(Status.NOT_STARTED.getDescription(), numberOfRecords.getValue0());
        chartSeries.set(Status.IN_PROGRESS.getDescription(), numberOfRecords.getValue1());
        chartSeries.set(Status.DELAYED.getDescription(), numberOfRecords.getValue2());
        chartSeries.set(Status.CONFIRMED_COMPLETED.getDescription(), numberOfRecords.getValue3());
        chartSeries.set(Status.UNCONFIRMED_COMPLETED.getDescription(), numberOfRecords.getValue4());
        this.businessSubProcessChatModel.addSeries(chartSeries);
    }

    private BigDecimal filterFindingForStatus(Collection<Finding> findings, Priority priority)
    {
        List<Finding> findingLists = Lists.newArrayList();
        for (Finding finding : findings)
        {
            if (priority.equals(finding.getPriority()))
            {
                findingLists.add(finding);
            }
        }
        return  new BigDecimal(findingLists.size());
    }

    private void findSelectedFindings(String priority, String status)
    {
        this.selectedFindings = this.summaryReportService.findSelectedFindings(auditType, user, priority, status);
        this.countOfDelayComparator();
    }

    public void exportFindingsToExcel()
    {
        LOGGER.debug("started to prepare excel {}", this.selectedFindings.size());
        Workbook workbook = this.summaryReportExcelService.prepareExcelReportOfTheFindings(this.selectedFindings);
        LOGGER.debug("workbook size is {}", workbook.getSheet("Findings").getLastRowNum());
        sendFileToBrowser(workbook);
    }

     public String sendFileToBrowser(Workbook workbook)
     {
        try
        {
            String filename = "findings.xls";

            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = facesContext.getExternalContext();
            externalContext.setResponseContentType("application/vnd.ms-excel");

            // http://stackoverflow.com/questions/3592058/how-to-send-byte-as-pdf-to-browser-in-java-web-application
            // attachment (pops up a "Save As" dialogue) or inline (let the web browser handle the display itself)
            externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

            workbook.write(externalContext.getResponseOutputStream()); //get workbook
            facesContext.responseComplete(); //if I don't call responseComplete() => IllegalStateException

            return null; //remain on same page
        }
        catch (Exception e)
        {
            // handle exception...
            return null; //remain on same page
        }
    }

    public void countOfDelayComparator()
    {
        Collections.sort(this.selectedFindings, new Comparator<Finding>()
        {
            @Override
            public int compare(Finding f1, Finding f2) {
                if (f1.getCountOfDelayedDay() > f2.getCountOfDelayedDay())
                {
                    return -1;
                }
                if (f1.getCountOfDelayedDay() == f2.getCountOfDelayedDay())
                {
                    return 0;
                }
                return 1;
            }
        });
    }
}
