package com.tr.aksigorta.denetimtaleptakip.controller.admin;

import com.google.common.collect.Lists;
import com.tr.aksigorta.denetimtaleptakip.model.Company;
import com.tr.aksigorta.denetimtaleptakip.model.Finding;
import com.tr.aksigorta.denetimtaleptakip.model.Status;
import lombok.Getter;
import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.PieChartModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * User: mhazer
 * Date: 8/30/12
 * Time: 11:21 PM
 */
@ManagedBean(name = "numberOfFindingsPerYearGraphicalReportController")
@RequestScoped
public class NumberOfFindingsPerYearGraphicalReportController extends BaseAdminController implements Serializable
{

    @Getter
    private PieChartModel pieChartModel;

    @PostConstruct
    public void init()
    {
        List<Finding> findings = this.findingService.retrieveAllFindings();
        pieChartModel = preparePieChart(findings);
    }

    public static PieChartModel preparePieChart(List<Finding> findings)
    {
        DateFormat df = new SimpleDateFormat("yyyy");

        PieChartModel _pieChartModel = new PieChartModel();
        for (Finding finding : findings)
        {
            String year = df.format(finding.getFindingYear());
            if (_pieChartModel.getData().get(year) == null)
            {
                _pieChartModel.set(year, 1);
            }
            else
            {
                Number oldValue = _pieChartModel.getData().get(year);
                Number newValue = (oldValue.intValue()) + 1;
                _pieChartModel.set(year, newValue);
            }
                
        }
        return _pieChartModel;
    }

    public void selectPieChart(ItemSelectEvent event) throws IOException
    {
        String yearOfSelectedChartItem = Lists.newLinkedList(pieChartModel.getData().entrySet()).get(event.getItemIndex()).getKey();
        logger.debug("clicked chart item is {}", yearOfSelectedChartItem);
        FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/admin/searchFinding.xhtml?findingYear=" + yearOfSelectedChartItem);
    }

    @Override
    protected Class getClazz()
    {
        return NumberOfFindingsPerYearGraphicalReportController.class;
    }
}
