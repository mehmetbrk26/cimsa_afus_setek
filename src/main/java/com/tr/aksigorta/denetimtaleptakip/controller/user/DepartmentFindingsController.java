package com.tr.aksigorta.denetimtaleptakip.controller.user;

import com.tr.aksigorta.denetimtaleptakip.controller.Visit;
import com.tr.aksigorta.denetimtaleptakip.model.Finding;
import com.tr.aksigorta.denetimtaleptakip.services.FindingService;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * User: mhazer
 * Date: 8/21/12
 * Time: 1:05 PM
 */
@ManagedBean(name = "departmentFindingsController")
@ViewScoped
public class DepartmentFindingsController implements Serializable
{
    @ManagedProperty("#{findingService}")
    @Setter
    private FindingService findingService;

    @ManagedProperty("#{visit}")
    @Setter
    private Visit visit;

    @Getter
    private LazyDataModel<Finding> findingLazyDataModel;

    @PostConstruct
    public void init()
    {
        findingLazyDataModel = new LazyDataModel<Finding>() {
            @Override
            public List<Finding> load(int first, int pageSize,
                                      String sortField, SortOrder sortOrder,
                                      Map<String, String> filters) {
                setRowCount(findingService.retrieveUserResponsibleDepartmentsFindingActionsCount(visit.getUser()).intValue());
                return findingService.retrieveUserResponsibleDepartmentsFindingActions(first, pageSize, sortField, sortOrder, visit.getUser());
            }
        };
    }
}
