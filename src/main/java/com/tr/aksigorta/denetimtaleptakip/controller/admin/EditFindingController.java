package com.tr.aksigorta.denetimtaleptakip.controller.admin;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.tr.aksigorta.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.aksigorta.denetimtaleptakip.model.Finding;
import com.tr.aksigorta.denetimtaleptakip.model.FindingAction;
import com.tr.aksigorta.denetimtaleptakip.model.User;
import com.tr.aksigorta.denetimtaleptakip.services.FindingService;
import com.tr.aksigorta.denetimtaleptakip.services.MailService;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:03 PM
 */
@ManagedBean(name = "editFindingController")
@ViewScoped
public class EditFindingController implements Serializable
{
    private final static Logger LOGGER = LoggerFactory.getLogger(EditFindingController.class);

    @ManagedProperty("#{findingService}")
    @Setter
    private FindingService findingService;

    @ManagedProperty("#{mailService}")
    @Setter
    private MailService mailService;

    @ManagedProperty("#{visit.user}")
    @Setter
    private User user;

    @ManagedProperty("#{commonDefinitionsController.activeUsers}")
    @Setter
    private List<User> users;

    @ManagedProperty("#{findingFileController}")
    @Setter
    private FindingFileController findingFileController;

    @Getter
    @Setter
    private Integer findingIdToUpdate;

    @Getter
    @Setter
    private DualListModel<User> possibleActionOwners;

    @Setter
    @Getter
    private DualListModel<User> possibleReadOnlyActionOwners;

    @Getter
    private Finding finding;

    @PostConstruct
    public void init()
    {
        this.finding = new Finding();
        this.finding.setAuditDate(new Date());
        this.finding.setAuditUser(this.user);
        preparePossibleActionOwnerDualListModel();
    }

    private void preparePossibleActionOwnerDualListModel()
    {
        List<User> usersSource = Lists.newLinkedList();
        List<User> usersTarget = Lists.newLinkedList();
        usersSource.addAll(users);
        possibleActionOwners = new DualListModel<User>(usersSource, usersTarget);
        List<User> possibleReadOnlyActionOwnersSource = Lists.newLinkedList();
        possibleReadOnlyActionOwnersSource.addAll(users);
        List<User> possibleReadOnlyActionOwnersTarget = Lists.newLinkedList();
        possibleReadOnlyActionOwners = new DualListModel<User>(possibleReadOnlyActionOwnersSource, possibleReadOnlyActionOwnersTarget);
    }

    public void prepareFindingForUpdate()
    {
        if (!FacesContext.getCurrentInstance().isPostback() && findingIdToUpdate != null)
        {
            LOGGER.debug("preparing finding for update: " + findingIdToUpdate);
            this.finding = this.findingService.retrieveFinding(findingIdToUpdate);
            Preconditions.checkNotNull(this.finding, "Can not find finding with the given id :", findingIdToUpdate);
            adjustActionOwners();
        }
    }

    private void adjustActionOwners()
    {
        for(FindingAction findingAction : this.finding.getFindingActions())
        {
            User findingActionUser = findingAction.getActionOwner();
            if(possibleActionOwners.getSource().contains(findingActionUser) || possibleReadOnlyActionOwners.getSource().contains(findingActionUser))
            {
                if (!findingAction.isReadOnly())
                {
                    possibleActionOwners.getSource().remove(findingActionUser);
                    possibleActionOwners.getTarget().add(findingActionUser);
                }
                else
                {
                    possibleReadOnlyActionOwners.getSource().remove(findingActionUser);
                    possibleReadOnlyActionOwners.getTarget().add(findingActionUser);
                }
            }
        }
    }

    public void changeAuditType()
    {
        LOGGER.debug("changing audit type");
        this.finding.setBusinessProcessType(null);
        this.finding.setBusinessSubProcessType(null);
        if (this.finding.getAuditType() != null)
        {
            this.findingService.reattachReadOnly(this.finding.getAuditType());
        }
        else
        {
            this.finding.setBusinessProcessType(null);
            FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "You have to choose audit type first", null);
        }
    }

    public void changeBusinessProcess()
    {
        LOGGER.debug("changing business process");
        this.finding.setBusinessSubProcessType(null);
        if (this.finding.getAuditType() != null)
        {
            this.findingService.reattachReadOnly(this.finding.getBusinessProcessType());
        }
        else
        {
            this.finding.setBusinessProcessType(null);
            FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "You have to choose audit type first", null);
        }
    }

    public void changeSubBusinessProcess()
    {
        LOGGER.debug("changing sub business process");
        this.findingService.reattachReadOnly(this.finding.getBusinessSubProcessType());
    }

    public String mergeFinding()
    {
        try
        {
            LOGGER.debug("creating new finding");
            LOGGER.debug("finding is: {}", this.finding.getFindingNumber());
            LOGGER.debug("Selected action owners are: {}", possibleActionOwners.getTarget());
            LOGGER.debug("Selected read only action owners are: {}", possibleReadOnlyActionOwners.getTarget());
            if (this.finding != null && this.finding.getId() == null)
            {
            	final Finding retrieveFinding = findingService.retrieveFinding(this.finding.getFindingNumber());
                Preconditions.checkArgument((new Date()).before(this.finding.getCompletionDate()), "Completion date must be after today");
                Preconditions.checkArgument(retrieveFinding==null, "Finding Number is duplicated!");
            }
            this.finding.adjustFindingActionsForUsers(possibleActionOwners.getTarget(), possibleReadOnlyActionOwners.getTarget());
            this.finding = this.findingService.mergeFinding(this.finding);
            this.sendOpeningEmails(finding);
            FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, this.finding.getAuditType().getDescription() + " finding created/updated successfully", null);
            return "/admin/findingAdmin.xhtml?faces-redirect=true";
        }
        catch (Exception exception)
        {
            LOGGER.error("can not save/update finding: {} ({})", finding, exception.getMessage());
            FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Can not save/update finding: " + exception.getMessage(), null);
        }
        return null;
    }

    private void sendOpeningEmails(Finding finding)
    {
        this.mailService.sendOpeningEmailToFindingActionOwnersAndResponsibleHeads(finding, finding.getAuditType().getOpeningEmailContent().getMailContent());
    }

    public void actionOwnerPickListOnTransfer(TransferEvent event)
    {
        if(event.isAdd())
        {
            List<User> addedUsers = (List<User>) event.getItems();
            for(User user : addedUsers)
            {
                if(!"readOnly".equals(event.getComponent().getAttributes().get("type")))
                {
                    FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, user.getNameSurname() + " assigned as action owner", null);
                }
                else
                {
                    FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, user.getNameSurname() + " assigned as read only action owner", null);
                }
            }
        }
        else if(event.isRemove())
        {
            List<User> removedUsers = (List<User>) event.getItems();
            for(User user : removedUsers)
            {
                if(!"readOnly".equals(event.getComponent().getAttributes().get("type")))
                {
                    FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, user.getNameSurname() + " removed from action owners", null);
                }
                else
                {
                    FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, user.getNameSurname() + " removed from read only action owners", null);
                }
            }
        }
    }

    public void validateCompletionDate(FacesContext context, UIComponent component, Object value) throws ValidatorException
    {
        if (this.finding != null && this.finding.getId() == null)
        {
            Date today = new Date();
            Date completionDate = (Date) value;
            if (completionDate.before(today))
            {
                FacesMessage msg = new FacesMessage("Completion date must be after today", "Completion date must be after today");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                ((UIInput) component).setValid(false);
                throw new ValidatorException(msg);
            }
        }
    }

    public void handleFileUpload(FileUploadEvent fileUploadEvent)
    {
        findingFileController.handleFileUpload(fileUploadEvent.getFile(), this.finding, this.user);
    }
}
