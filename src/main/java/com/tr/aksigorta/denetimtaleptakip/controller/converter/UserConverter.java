package com.tr.aksigorta.denetimtaleptakip.controller.converter;

import com.tr.aksigorta.denetimtaleptakip.model.User;
import org.apache.commons.lang.StringUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.List;

/**
 * User: mhazer
 * Date: 8/20/12
 * Time: 8:43 AM
 */
@FacesConverter(value = "userConverter")
public class UserConverter implements Converter
{
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String id)
    {
        List<User> users = (List<User>) uiComponent.getAttributes().get("userEntities");
        if (users != null)
        {
            for (User user : users)
            {
                if (user.getId().toString().equals(id))
                {
                    return user;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o)
    {
        User user = (User) o;
        return user != null ? user.getId().toString() : StringUtils.EMPTY;
    }
}
