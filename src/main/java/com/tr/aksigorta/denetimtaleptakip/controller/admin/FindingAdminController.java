package com.tr.aksigorta.denetimtaleptakip.controller.admin;

import com.tr.aksigorta.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.aksigorta.denetimtaleptakip.model.Finding;
import com.tr.aksigorta.denetimtaleptakip.model.FindingAction;
import com.tr.aksigorta.denetimtaleptakip.model.FindingResponsibleAction;
import com.tr.aksigorta.denetimtaleptakip.model.User;
import com.tr.aksigorta.denetimtaleptakip.services.FindingExcelService;
import com.tr.aksigorta.denetimtaleptakip.services.FindingService;
import com.tr.aksigorta.denetimtaleptakip.services.MailService;

import lombok.Getter;
import lombok.Setter;
import org.apache.poi.ss.usermodel.Workbook;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.chart.PieChartModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * User: mhazer
 * Date: 8/20/12
 * Time: 2:28 PM
 */
@ManagedBean(name = "findingAdminController")
@ViewScoped
public class FindingAdminController implements Serializable
{
    private final static Logger logger = LoggerFactory.getLogger(FindingAdminController.class);

    @ManagedProperty("#{findingService}")
    private FindingService findingService;

    @ManagedProperty("#{findingExcelService}")
    private FindingExcelService findingExcelService;
    
    @ManagedProperty("#{mailService}")
    @Setter
    private MailService mailService;

    @Getter
    @Setter
    private Finding selectedFinding;

    @Getter
    @Setter
    private FindingAction selectedFindingAction;
    
    @Getter
    @Setter
    private List<FindingAction> selectedFindingActions;

    @Getter
    @Setter
    private FindingResponsibleAction selectedFindingResponsibleAction;

    @Getter
    private PieChartModel pieChartModel;

    @ManagedProperty("#{visit.user}")
    @Setter
    private User user;

    @Getter
    private LazyDataModel<Finding> findingLazyDataModel;

    @PostConstruct
    public void init()
    {
        findingLazyDataModel = new LazyDataModel<Finding>() {
            @Override
            public List<Finding> load(int first, int pageSize,
                                      String sortField, SortOrder sortOrder,
                                      Map<String, String> filters) {
                this.setRowCount(findingService.retrieveAllFindingsCount().intValue());
                return findingService.retrieveAllFindings(first, pageSize, sortField, sortOrder);
            }
        };
    }

    public void selectFinding(Finding finding)
    {
        this.selectedFinding = finding;
        this.selectedFindingAction = new FindingAction();
        this.selectedFindingAction.setActionTaken("");
        this.selectedFindingAction.setFinding(selectedFinding);
        this.selectedFindingResponsibleAction = null;
    }

    public void selectFindingForConfirmation(Finding finding)
    {
        this.selectedFinding = finding;
        this.selectedFindingResponsibleAction = new FindingResponsibleAction();
    }
    
    public void selectFindingForEditUserActions(Finding finding)
    {
        this.selectedFinding = finding;
        this.selectedFindingAction = new FindingAction();
        this.selectedFindingAction.setActionTaken("");
        this.selectedFindingAction.setFinding(selectedFinding);
        this.selectedFindingActions = finding.findingActionsAsList();
    }

    public void selectFindingAction(FindingAction findingAction)
    {
        findingAction.setHistoryOfFindingAction(this.findingService.retrieveHistoryOfFindingAction(findingAction.getId()));
        this.selectedFindingAction = findingAction;
    }

    public void selectFindingResponsibleAction(FindingResponsibleAction findingResponsibleAction)
    {
        findingResponsibleAction.setHistoryOfFindingResponsibleAction(this.findingService.retrieveHistoryOfFindingResponsibleAction(findingResponsibleAction.getId()));
        this.selectedFindingResponsibleAction = findingResponsibleAction;
    }

    private boolean isFindingResponsibleActionExistsForUser(User _user)
    {
        final List<FindingResponsibleAction> findingResponsibleActions = this.getSelectedFinding().findingResponsibleActionsAsList();
        for (FindingResponsibleAction findingResponsibleAction : findingResponsibleActions)
        {
            if (findingResponsibleAction.getActionOwner().getId().equals(_user.getId()))
            {
                return true;
            }
        }
        return false;
    }

    public void updateFindingResponsibleAction()
    {
        try
        {
            checkResponsibleFindingActionValues();
            if(this.selectedFinding.isFindingResponsibleActionExistsForUser(this.user)){
                editFindingResponsibleAction();
            }else{
                addNewFindingResponsibleUser();
            }
            //this.sendUpdateEmails(this.selectedFindingAction);
        }
        catch (Exception exception)
        {
            FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "Finding status update failed: (" + exception.getMessage() + ")", null);
        }
        finally
        {
            this.selectedFindingResponsibleAction = null;
            this.init();
        }
    }

    private void addNewFindingResponsibleUser() {
        if(isUserResponsibleForFinding(this.selectedFinding, user)){
            FindingResponsibleAction findingResponsibleAction = new FindingResponsibleAction();
            findingResponsibleAction.setChangeDate(new Date());
            findingResponsibleAction.setActionTaken(this.selectedFindingResponsibleAction.getActionTaken());
            findingResponsibleAction.setCompletionPercentage(this.selectedFindingResponsibleAction.getCompletionPercentage());
            findingResponsibleAction.setExplanationForDelay(this.selectedFindingResponsibleAction.getExplanationForDelay());
            findingResponsibleAction.setRevisedCompletionDate(this.selectedFindingResponsibleAction.getRevisedCompletionDate());
            findingResponsibleAction.setFinding(this.selectedFinding);
            findingResponsibleAction.setActionOwner(this.user);
            this.findingService.mergeFindingResponsibleAction(findingResponsibleAction);
            FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Finding status updated successfully", null);
        }else{
            FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "You are not responsible owner for this finding", null);
        }
    }

    private void editFindingResponsibleAction() {
        if(isUserResponsibleForFinding(this.selectedFinding, user)){
            FindingResponsibleAction findingResponsibleAction = this.selectedFinding.findingResponsibleActionOfUser(this.user);
            findingResponsibleAction.setChangeDate(new Date());
            findingResponsibleAction.setActionTaken(this.selectedFindingResponsibleAction.getActionTaken());
            findingResponsibleAction.setCompletionPercentage(this.selectedFindingResponsibleAction.getCompletionPercentage());
            findingResponsibleAction.setExplanationForDelay(this.selectedFindingResponsibleAction.getExplanationForDelay());
            findingResponsibleAction.setRevisedCompletionDate(this.selectedFindingResponsibleAction.getRevisedCompletionDate());
            findingResponsibleAction.setFinding(this.selectedFinding);
            findingResponsibleAction.setActionOwner(this.user);
            this.findingService.mergeFindingResponsibleAction(findingResponsibleAction);
            FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Finding status updated successfully", null);
        }else{
            FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "You are not responsible owner for this finding anymore", null);
        }
    }

    private boolean isUserResponsibleForFinding(Finding selectedFinding, User user) {
        return user.getResponsibleBusinessTypes().contains(selectedFinding.getBusinessProcessType()) ||
                user.getResponsibleBusinessSubTypes().contains(selectedFinding.getBusinessSubProcessType());
    }

    private void checkResponsibleFindingActionValues()
    {
        if (this.selectedFindingResponsibleAction.getCompletionPercentage() != 100)
        {
            if (this.selectedFindingResponsibleAction.getRevisedCompletionDate() != null && this.selectedFindingResponsibleAction.getRevisedCompletionDate().before(new Date()))
            {
                //throw new IllegalArgumentException("You need to choose revised date after today");
            }
        }
    }


    public String updateFinding(Finding finding)
    {
        return "/admin/editFinding.xhtml?faces-redirect=true&findingId=" + finding.getId();
    }

    public void reviseFinding(Finding finding)
    {
        if(isUserResponsibleForFinding(finding, user)){
            finding.setRevised(true);
            this.findingService.mergeFinding(finding);
            FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Revised completion date is approved successfully", null);
        }else{
            FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "You are not responsible owner for this finding", null);
        }
    }

    public String reassignFinding(Finding finding)
    {
        return "/admin/reassignFinding.xhtml?faces-redirect=true&findingId=" + finding.getId();
    }

    public void exportFindingsToExcel(List<Finding> findings)
    {
        logger.debug("started to prepare excel {}", findings.size());
        Workbook workbook = this.findingExcelService.prepareExcelReportOfTheFindings(findings);
        logger.debug("workbook size is {}", workbook.getSheet("Findings").getLastRowNum());
        sendFileToBrowser(workbook);
    }

    public String sendFileToBrowser(Workbook workbook)
    {
        try
        {
            String filename = "findings.xls";

            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = facesContext.getExternalContext();
            externalContext.setResponseContentType("application/vnd.ms-excel");

            // http://stackoverflow.com/questions/3592058/how-to-send-byte-as-pdf-to-browser-in-java-web-application
            // attachment (pops up a "Save As" dialogue) or inline (let the web browser handle the display itself)
            externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

            workbook.write(externalContext.getResponseOutputStream()); //get workbook
            facesContext.responseComplete(); //if I don't call responseComplete() => IllegalStateException

            return null; //remain on same page
        }
        catch (Exception e)
        {
            // handle exception...
            return null; //remain on same page
        }
    }

    public void preparePieChart(List<Finding> findings)
    {
        logger.debug("preparing pie chart");
        try
        {
            pieChartModel = NumberOfFindingsPerStatusGraphicalReportController.preparePieChart(findings);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "We couldn't prepare the graph of findings" + exception.getMessage(), null);
        }
    }

    public void setFindingService(FindingService findingService)
    {
        this.findingService = findingService;
    }

    public void setFindingExcelService(FindingExcelService findingExcelService)
    {
        this.findingExcelService = findingExcelService;
    }
    
    public void deleteFinding(Finding finding)
    {
    	logger.debug("preparing to delete finding");
        try
        {
        	findingService.deleteFinding(finding);
        	FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Finding deleted successfully", null);
        }        
        catch (Exception exception)
        {
            exception.printStackTrace();
            FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "Error occured while deleting finding" + exception.getMessage(), null);
        }
    }
    
    public void updateFindingActions()
    {
    	try
        {
            for( FindingAction action : this.selectedFindingActions)
	    	{
	    		if(!action.isReadOnly())
	    		{
	    			action.setActionTaken("["+user.getNameSurname()+" tarafından] "+this.selectedFindingAction.getActionTaken());
	    			action.setCompletionPercentage(this.selectedFindingAction.getCompletionPercentage());
	    			action.setExplanationForDelay(this.selectedFindingAction.getExplanationForDelay());
	    			action.setRevisedCompletionDate(this.selectedFindingAction.getRevisedCompletionDate());
	    			updateFindingAction(action);
	    		}
	    	}
	    	FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Finding status is updated successfully", null);
	    }
    	catch (Exception exception)
		{
		    exception.printStackTrace();
		    FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "Finding status update is failed: (" + exception.getMessage() + ")", null);
		}
		finally
		{
		    this.selectedFindingAction = null;
		    this.selectedFinding = null;
		    this.init();
		}
    }
    
    public void updateFindingAction(FindingAction findingAction)
    {
    	findingAction.setChangeDate(new Date());
        this.findingService.mergeFindingAction(findingAction);
        this.sendUpdateEmails(findingAction);
    }
    
    private void sendUpdateEmails(FindingAction findingAction)
    {
        this.mailService.sendFindingActionUpdateEmail(findingAction);
    }
}
