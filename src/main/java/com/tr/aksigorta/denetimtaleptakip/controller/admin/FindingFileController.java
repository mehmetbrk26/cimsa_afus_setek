package com.tr.aksigorta.denetimtaleptakip.controller.admin;

import com.tr.aksigorta.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.aksigorta.denetimtaleptakip.model.Finding;
import com.tr.aksigorta.denetimtaleptakip.model.FindingFile;
import com.tr.aksigorta.denetimtaleptakip.model.User;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;

/**
 * User: mhazer
 * Date: 5/21/13
 * Time: 9:12 AM
 */
@ManagedBean(name = "findingFileController")
@ViewScoped
public class FindingFileController implements Serializable
{
    private final static Logger LOGGER = LoggerFactory.getLogger(EditFindingController.class);

    public void handleFileUpload(UploadedFile uploadedFile, Finding finding, User user)
    {
        try
        {
            FindingFile findingFile = FindingFile.createInstance(finding, user, uploadedFile);
            finding.getFindingFiles().add(findingFile);
            FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, uploadedFile.getFileName() + " named file uploaded successfully", null);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            LOGGER.error("error occurred during adding file to finding ({})", exception.getMessage());
            FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "Error occured during  uploading file (" + exception.getMessage() + ")", null);
        }
    }

    public void removeFile(FindingFile findingFile, Finding finding)
    {
        finding.getFindingFiles().remove(findingFile);
    }

    public StreamedContent viewFile(FindingFile file)
    {

        try
        {
            InputStream stream = new ByteArrayInputStream(file.getContent());
            DefaultStreamedContent streamedContent = new DefaultStreamedContent(stream, file.getContentType(), file.getFileName());
            return streamedContent;
        }
        catch (Exception exception)
        {
            FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "Error occured during downloading file (" + exception.getMessage() + ")", null);
        }

        return null;
    }
}
