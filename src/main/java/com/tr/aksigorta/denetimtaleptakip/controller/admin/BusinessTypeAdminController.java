package com.tr.aksigorta.denetimtaleptakip.controller.admin;

import com.google.common.collect.Lists;
import com.tr.aksigorta.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.aksigorta.denetimtaleptakip.model.BusinessProcessType;
import com.tr.aksigorta.denetimtaleptakip.model.BusinessSubProcessType;
import com.tr.aksigorta.denetimtaleptakip.model.User;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.DualListModel;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * User: mhazer
 * Date: 8/30/12
 * Time: 12:50 PM
 */
@ManagedBean(name = "businessTypeAdminController")
@ViewScoped
public class BusinessTypeAdminController extends BaseAdminController implements Serializable
{
    @Getter
    @Setter
    private BusinessProcessType selectedBusinessProcessType;

    @Getter
    @Setter
    private DualListModel<User> possibleBusinessProcessResposibleHeads;

    @Getter
    @Setter
    private DualListModel<User> possibleBusinessSubProcessResposibleHeads;

    @Setter
    @Getter
    private BusinessSubProcessType selectedBusinessSubProcessType;

    public void selectBusinessProcessType(BusinessProcessType businessProcessType)
    {
        this.findingService.reattachReadOnly(businessProcessType);
        this.selectedBusinessProcessType = businessProcessType;
        this.selectedBusinessProcessType.getSubProcessTypes().size();
        prepareBusinessProcessReponsibleHeadsDualListModel();
        adjustBusinessProcessReponsibleHeadsDualListModel();
    }

    private void prepareBusinessProcessReponsibleHeadsDualListModel()
    {
        List<User> businessProcResonsibleHeadsSource = Lists.newLinkedList();
        List<User> businessProcResonsibleHeadsTarget = Lists.newLinkedList();
        businessProcResonsibleHeadsSource.addAll(commonDefinitionsController.getActiveUsers());
        possibleBusinessProcessResposibleHeads = new DualListModel<User>(businessProcResonsibleHeadsSource, businessProcResonsibleHeadsTarget);
    }

    private void adjustBusinessProcessReponsibleHeadsDualListModel()
    {
        for (User user : this.selectedBusinessProcessType.getResponsibleHeads())
        {
            if (possibleBusinessProcessResposibleHeads.getSource().contains(user))
            {
                possibleBusinessProcessResposibleHeads.getSource().remove(user);
                possibleBusinessProcessResposibleHeads.getTarget().add(user);
            }
        }
    }

    public void addNewBusinessType()
    {
        this.selectedBusinessProcessType = new BusinessProcessType();
        prepareBusinessProcessReponsibleHeadsDualListModel();
    }

    public void cancelBusinessProcessOperation()
    {
        this.commonDefinitionsController.refreshBusinessTypes();
    }

    public void saveBusinessType()
    {
        try
        {
            logger.debug("saving business type {}", this.selectedBusinessProcessType);
            this.selectedBusinessProcessType.setAdminUser(visit.getUser());
            this.selectedBusinessProcessType.setEditDate(new Date());
            this.findingService.mergeBusinessType(selectedBusinessProcessType, this.possibleBusinessProcessResposibleHeads.getTarget());
            this.commonDefinitionsController.refreshBusinessTypes();
            FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Business Type updated successfully", null);
        }
        catch (Exception exception)
        {
            FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Can not save Business Type (" + exception.getMessage() + ")", null);
        }
        finally
        {
            this.selectedBusinessProcessType = null;
        }
    }

    public void editBusinessSubProcessType(BusinessSubProcessType businessSubProcessType)
    {
        this.findingService.reattachReadOnly(businessSubProcessType);
        this.selectedBusinessSubProcessType = businessSubProcessType;
        prepareBusinessSubProcessReponsibleHeadsDualListModel();
        adjustBusinessSubProcessReponsibleHeadsDualListModel();
    }

    private void prepareBusinessSubProcessReponsibleHeadsDualListModel()
    {
        List<User> businessSubProcResonsibleHeadsSource = Lists.newLinkedList();
        List<User> businessSubProcResonsibleHeadsTarget = Lists.newLinkedList();
        businessSubProcResonsibleHeadsSource.addAll(commonDefinitionsController.getActiveUsers());
        possibleBusinessSubProcessResposibleHeads = new DualListModel<User>(businessSubProcResonsibleHeadsSource, businessSubProcResonsibleHeadsTarget);
    }

    private void adjustBusinessSubProcessReponsibleHeadsDualListModel()
    {
        for (User user : this.selectedBusinessSubProcessType.getResponsibleHeads())
        {
            if (possibleBusinessSubProcessResposibleHeads.getSource().contains(user))
            {
                possibleBusinessSubProcessResposibleHeads.getSource().remove(user);
                possibleBusinessSubProcessResposibleHeads.getTarget().add(user);
            }
        }
    }

    public void newBusinessSubProcessType()
    {
        this.selectedBusinessSubProcessType = new BusinessSubProcessType();
        prepareBusinessSubProcessReponsibleHeadsDualListModel();
    }

    public void saveNewBusinessSubType()
    {
        this.selectedBusinessSubProcessType.setAdminUser(visit.getUser());
        this.selectedBusinessSubProcessType.setEditDate(new Date());
        this.selectedBusinessSubProcessType.getResponsibleHeads().addAll(this.possibleBusinessSubProcessResposibleHeads.getTarget());
        if (this.selectedBusinessSubProcessType.getId() == null)
        {
            this.selectedBusinessSubProcessType.setMainProcess(this.selectedBusinessProcessType);
            this.selectedBusinessProcessType.getSubProcessTypes().add(this.selectedBusinessSubProcessType);
        }
        else
        {
            for(Iterator iterator = this.selectedBusinessSubProcessType.getResponsibleHeads().iterator(); iterator.hasNext();)
            {
                if(!this.possibleBusinessSubProcessResposibleHeads.getTarget().contains(iterator.next()))
                {
                    iterator.remove();
                }
            }
        }
    }

    @Override
    protected Class getClazz()
    {
        return BusinessTypeAdminController.class;
    }
}
