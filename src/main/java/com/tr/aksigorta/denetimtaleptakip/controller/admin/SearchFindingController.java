package com.tr.aksigorta.denetimtaleptakip.controller.admin;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.tr.aksigorta.denetimtaleptakip.controller.Visit;
import com.tr.aksigorta.denetimtaleptakip.controller.util.CommonDefinitionsController;
import com.tr.aksigorta.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.aksigorta.denetimtaleptakip.model.AuditType;
import com.tr.aksigorta.denetimtaleptakip.model.Company;
import com.tr.aksigorta.denetimtaleptakip.model.Finding;
import com.tr.aksigorta.denetimtaleptakip.services.FindingService;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * User: mhazer
 * Date: 8/20/12
 * Time: 2:28 PM
 */
@ManagedBean(name = "searchFindingController")
@ViewScoped
public class SearchFindingController implements Serializable
{
    @ManagedProperty("#{findingService}")
    @Setter
    private FindingService findingService;

    @ManagedProperty("#{commonDefinitionsController}")
    @Setter
    private CommonDefinitionsController commonDefinitionsController;

    @ManagedProperty("#{visit}")
    @Setter
    private Visit visit;

    @Getter
    private List<Finding> searchResults;

    @Getter
    private SearchParameters searchParameters;

    @Getter
    @Setter
    private boolean selectAllAuditTypes;

    @Getter
    @Setter
    private boolean selectAllCompanies;

    @Getter
    @Setter
    private boolean selectAllPriorities;

    @Getter
    @Setter
    private boolean selectAllBusinessTypes;

    @Getter
    @Setter
    private boolean selectAllBusinessSubTypes;

    @Getter
    @Setter
    private boolean selectAllActionOwners;

    @PostConstruct
    public void init()
    {
        searchParameters = new SearchParameters();
        
    }

    public void search()
    {
        try
        {
            this.searchResults = this.findingService.searchFindings(searchParameters, visit.getUser());
            Preconditions.checkArgument(CollectionUtils.isNotEmpty(this.searchResults), "There is not any findings suitable to your search criteria");
        }
        catch (Exception exception)
        {
            if( !(exception instanceof IllegalArgumentException) ) 
            	exception.printStackTrace();
            FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, exception.getMessage(), null);
        }
    }

    public void searchFromUrl()
    {
        if (!FacesContext.getCurrentInstance().isPostback() && searchParameters.isAtLeastOneSearchParameterFilled())
        {
            String companyName = searchParameters.getCompanyName();
            String findingYearStr = searchParameters.getFindingYearStr();
            if (StringUtils.isNotBlank(companyName)) {
                Company company = findingService.retrieveCompany(companyName);
                if (company != null) {
                    searchParameters.addCompany(company);
                }
            }
            if( StringUtils.isNotBlank(findingYearStr) )
            {
            	searchParameters.setFindingYear(new LocalDate(Integer.parseInt(findingYearStr),1,1).toDate());
            }
            search();
        }
    }

    public void clearForm()
    {
        searchParameters.clearFields();
        this.searchResults = null;
    }

    public void selectDeSelectAllAuditTypes()
    {
        if (this.selectAllAuditTypes)
        {
            this.searchParameters.getAuditTypes().addAll(this.commonDefinitionsController.getAuditTypes());
        }
        else
        {
            this.searchParameters.getAuditTypes().clear();
        }
    }
    
    public void selectDeSelectAllCompanies()
    {
        if (this.selectAllCompanies)
        {
            this.searchParameters.getCompanies().addAll(this.commonDefinitionsController.getCompanies());
        }
        else
        {
            this.searchParameters.getCompanies().clear();
        }
    }

    public void selectDeSelectAllPriorities()
    {
        if (this.selectAllPriorities)
        {
            this.searchParameters.getPriorities().addAll(this.commonDefinitionsController.getPriorities());
        }
        else
        {
            this.searchParameters.getPriorities().clear();
        }
    }

    public void selectDeSelectAllBusinessTypes()
    {
        if (this.selectAllBusinessTypes)
        {
            this.searchParameters.getBusinessProcessTypes().addAll(this.commonDefinitionsController.getBusinessProcessTypes());
        }
        else
        {
            this.searchParameters.getBusinessProcessTypes().clear();
        }
    }

    public void selectDeSelectAllBusinessSubTypes()
    {
        if (this.selectAllBusinessSubTypes)
        {
            this.searchParameters.getBusinessSubProcessTypes().addAll(this.commonDefinitionsController.getBusinessSubProcessTypes());
        }
        else
        {
            this.searchParameters.getBusinessSubProcessTypes().clear();
        }
    }

    public void selectDeSelectAllActionOwners()
    {
        if (this.selectAllActionOwners)
        {
            this.searchParameters.getUsers().addAll(this.commonDefinitionsController.getActiveUsers());
        }
        else
        {
            this.searchParameters.getUsers().clear();
        }
    }
}
