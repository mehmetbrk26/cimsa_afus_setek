package com.tr.aksigorta.denetimtaleptakip.controller.user;

import com.tr.aksigorta.denetimtaleptakip.controller.Visit;
import com.tr.aksigorta.denetimtaleptakip.controller.admin.FindingFileController;
import com.tr.aksigorta.denetimtaleptakip.controller.admin.NumberOfFindingsPerStatusGraphicalReportController;
import com.tr.aksigorta.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.aksigorta.denetimtaleptakip.model.Finding;
import com.tr.aksigorta.denetimtaleptakip.model.FindingAction;
import com.tr.aksigorta.denetimtaleptakip.model.FindingResponsibleAction;
import com.tr.aksigorta.denetimtaleptakip.services.FindingExcelService;
import com.tr.aksigorta.denetimtaleptakip.services.FindingService;
import com.tr.aksigorta.denetimtaleptakip.services.MailService;
import lombok.Getter;
import lombok.Setter;
import org.apache.poi.ss.usermodel.Workbook;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.chart.PieChartModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * User: mhazer
 * Date: 8/21/12
 * Time: 1:05 PM
 */
@ManagedBean(name = "userTasksController")
@ViewScoped
public class UserTasksController implements Serializable
{
    private final static Logger LOGGER = LoggerFactory.getLogger(UserTasksController.class);

    @ManagedProperty("#{findingService}")
    private FindingService findingService;

    @ManagedProperty("#{findingExcelService}")
    private FindingExcelService findingExcelService;

    @ManagedProperty("#{mailService}")
    @Setter
    private MailService mailService;

    @Getter
    private PieChartModel pieChartModel;

    @ManagedProperty("#{visit}")
    private Visit visit;

    @ManagedProperty("#{findingFileController}")
    @Setter
    private FindingFileController findingFileController;

    @Getter
    private FindingAction selectedFindingAction;

    @Getter
    public Finding selectedFinding;

    @Getter
    @Setter
    private FindingResponsibleAction selectedFindingResponsibleAction;

    @Getter
    private LazyDataModel<FindingAction> findingActionLazyDataModel;

    @PostConstruct
    public void init()
    {
        findingActionLazyDataModel = new LazyDataModel<FindingAction>() {
            @Override
            public List<FindingAction> load(int first, int pageSize,
                                            String sortField, SortOrder sortOrder,
                                            Map<String, String> filters) {
            	
            	if( sortField == null || sortField.equals("") )
            		sortField = "custom";
            	
                setRowCount(findingService.retrieveUserFindingActionsCount(visit.getUser()).intValue());
                return findingService.retrieveUserFindingActions(first, pageSize, sortField, sortOrder, visit.getUser());
            }
        };
    }

    public void selectFindingAction(FindingAction findingAction)
    {
        this.selectedFindingAction = this.findingService.retrieveFindingActionWithHistory(findingAction.getId());
        this.selectedFinding = this.selectedFindingAction.getFinding();
    }
    

/*    public void selectFindingResponsibleAction(FindingResponsibleAction findingResponsibleAction)
    {
        findingResponsibleAction.setHistoryOfFindingResponsibleAction(this.findingService.retrieveHistoryOfFindingResponsibleAction(findingResponsibleAction.getId()));
        this.selectedFindingResponsibleAction = findingResponsibleAction;
    }*/

    public void updateFindingAction()
    {
        try
        {
            checkFindingActionValues();
            if(this.selectedFindingAction.getFinding().getCompleted()
                    && this.selectedFindingAction.getFinding().getConfirmed()){
                FacesUtils.addMessage(FacesMessage.SEVERITY_WARN, "Finding is already confirmed/completed. Finding status can not be updated.", null);
            }else{
                this.selectedFindingAction.setChangeDate(new Date());
                this.findingService.mergeFindingAction(this.selectedFindingAction);
                this.sendUpdateEmails(this.selectedFindingAction);
                FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Finding status is updated successfully", null);
            }
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "Finding status update is failed: (" + exception.getMessage() + ")", null);
        }
        finally
        {
            this.selectedFindingAction = null;
            this.selectedFinding = null;
            this.init();
        }
    }

    public void updateFindingComments()
    {
        try
        {
            final Finding finding = this.selectedFindingAction.getFinding();

            if (finding != null && finding.getId() != null)
            {
                LOGGER.debug("finding is: {}", finding.getFindingNumber());
                this.findingService.mergeFinding(finding);
                FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Finding comments are updated successfully", null);
            }else{
                FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "Finding is not found.", null);
            }
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "Finding comments update is failed: (" + exception.getMessage() + ")", null);
        }
        finally
        {
            this.selectedFindingAction = null;
            this.selectedFinding = null;
            this.init();
        }
    }

    private void sendUpdateEmails(FindingAction findingAction)
    {
        this.mailService.sendFindingActionUpdateEmail(findingAction);
    }

    private void checkFindingActionValues()
    {
        if (this.selectedFindingAction.getCompletionPercentage() != 100)
        {
            if (this.selectedFindingAction.getRevisedCompletionDate() != null && this.selectedFindingAction.getRevisedCompletionDate().before(new Date()))
            {
                //throw new IllegalArgumentException("You need to choose revised date after today");
            }
        }
       /** 15.10.2018 Durhasan kapatıldı
        if( this.selectedFindingAction.getCompletionPercentage() == 100 &&
        		this.selectedFindingAction.getRevisedCompletionDate() != null )
        {
        	throw new IllegalArgumentException("Bulgu tamamlanma yüzdesi %100 olduğundan revize tamamlanma tarihine ihtiyaç yoktur.");
        }
        
        if( this.selectedFinding.getCompletionPercentage()>0 &&  this.selectedFinding.getCompletionPercentage()<100 &&
        		this.selectedFindingAction.getRevisedCompletionDate() == null)
        {
        	throw new IllegalArgumentException("Lütfen Revize Tamamlanma Tarihi Giriniz.");
        }**/
        	
    }

    public void handleFileUpload(FileUploadEvent fileUploadEvent)
    {
        findingFileController.handleFileUpload(fileUploadEvent.getFile(), this.selectedFindingAction.getFinding(), this.visit.getUser());
    }

    public void setFindingService(FindingService findingService)
    {
        this.findingService = findingService;
    }

    public void setFindingExcelService(FindingExcelService findingExcelService)
    {
        this.findingExcelService = findingExcelService;
    }

    public void setVisit(Visit visit)
    {
        this.visit = visit;
    }

    public void exportFindingsToExcel(List<Finding> findings)
    {
        LOGGER.debug("started to prepare excel {}", findings.size());
        Workbook workbook = this.findingExcelService.prepareExcelReportOfTheFindings(findings);
        LOGGER.debug("workbook size is {}", workbook.getSheet("Findings").getLastRowNum());
        sendFileToBrowser(workbook);
    }

    public String sendFileToBrowser(Workbook workbook)
    {
        try
        {
            String filename = "findings.xls";

            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = facesContext.getExternalContext();
            externalContext.setResponseContentType("application/vnd.ms-excel");

            // http://stackoverflow.com/questions/3592058/how-to-send-byte-as-pdf-to-browser-in-java-web-application
            // attachment (pops up a "Save As" dialogue) or inline (let the web browser handle the display itself)
            externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

            workbook.write(externalContext.getResponseOutputStream()); //get workbook
            facesContext.responseComplete(); //if I don't call responseComplete() => IllegalStateException

            return null; //remain on same page
        }
        catch (Exception e)
        {
            // handle exception...
            return null; //remain on same page
        }
    }

    public void preparePieChart(List<Finding> findings)
    {
        LOGGER.debug("preparing pie chart");
        try
        {
            pieChartModel = NumberOfFindingsPerStatusGraphicalReportController.preparePieChart(findings);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "We couldn't prepare the graph of findings" + exception.getMessage(), null);
        }
    }
}
